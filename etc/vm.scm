;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with rde.  If not, see <http://www.gnu.org/licenses/>.
;;;

;; Generate a bootable image (e.g. for USB sticks, etc.) with:
;; $ guix system image -t qemu vm.scm

(define-module (etc vm)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages package-management)
  #:use-module (nongnu packages linux)
  #:use-module (guix)
  #:export (vm-os-nonfree))

(define installation-os-nonfree
  (operating-system
    (inherit installation-os)
    (kernel linux)
    (firmware (list linux-firmware))

    ;; Add the 'net.ifnames' argument to prevent network interfaces
    ;; from having really long names.  This can cause an issue with
    ;; wpa_supplicant when you try to connect to a wifi network.
    (kernel-arguments '("quiet" "modprobe.blacklist=radeon" "net.ifnames=0"))

    (services
     (cons*
      ;; Include the channel file so that it can be used during installation
      (simple-service 'channel-file etc-service-type
                      (list `("channels.scm" ,(local-file "channels.scm"))))
      (modify-services (operating-system-user-services installation-os)
        (guix-service-type
         config => (guix-configuration
                    (inherit config)
                    (authorize-key? #t)
                    ;; Install and run the current Guix rather than an older
                    ;; snapshot.
                    (guix (current-guix))
                    (substitute-urls (append
                                      %default-substitute-urls
                                      (list "https://substitutes.nonguix.org")))
                    (authorized-keys (append
                                      (list
                                       (local-file "keys/nonguix.pub")
                                       %default-authorized-guix-keys)))))
        )))

    ;; Add some extra packages useful for the installation process
    (packages
     (append (list git curl neovim emacs-no-x-toolkit)
             (operating-system-packages installation-os)))))
