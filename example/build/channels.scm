(cons* (channel
         (name 'dinos)
         (url "https://gitlab.com/ody55eus/dinos")
         (branch "main")
         (introduction
          (make-channel-introduction
           "e802e65a76e49435ded5ef909120b2b84f2e262b"
           (openpgp-fingerprint
            "885B 941B 8221 3321 6D96 0E4C DE2A D6CF 2474 B880"))))
       %default-channels)
