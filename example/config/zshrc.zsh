#!/usr/bin/env zsh

[[ $- == *i* ]] && stty stop undef    # Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')

# Include external sources
SHAREDIR=$HOME/.guix-home/profile/share/zsh
[ -e $SHAREDIR/plugins/zsh-syntax-highlighting ] && source $SHAREDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -e $SHAREDIR/plugins/zsh-autopair ] && source $SHAREDIR/plugins/zsh-autopair/zsh-autopair.zsh
[ -e $SHAREDIR/plugins/zsh-autosuggestions ] && source $SHAREDIR/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -e $HOME/.cache/zsh/ohmyzsh/custom/plugins/zsh-completions/src ] && fpath+="$HOME/.cache/zsh/ohmyzsh/custom/plugins/zsh-completions/src"
[ -e $HOME/.config/guix/current/share/zsh/site-functions ] && fpath+="$HOME/.config/guix/current/share/zsh/site-functions"

# Base Options
# some useful options (man zshoptions)
# setopt interactive_comments

setopt autocd              # change directory just by typing its name
#setopt correct            # auto correct mistakes
setopt interactivecomments # allow comments in interactive mode
setopt magicequalsubst     # enable filename expansion for arguments of the form ‘anything=expression’
setopt nonomatch           # hide error message if there is no match for the pattern
setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense
setopt promptsubst         # enable command substitution in prompt
setopt extendedglob        # use additional pattern matching features
#setopt incappendhistory # Save history to shared file, but not read
setopt sharehistory     # Share history across shell sessions
setopt histignorespace  # Ignore commands that start with space
unsetopt BEEP              # beeping is annoying

# Configuring help (M-h to call it on current command/function)
autoload -Uz run-help
(( ${+aliases[run-help]} )) && unalias run-help
autoload -Uz run-help-git

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

# Colors
autoload -Uz colors && colors
LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=00:tw=30;42:ow=31;32:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.avif=01;35:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:*~=00;90:*#=00;90:*.bak=00;90:*.old=00;90:*.orig=00;90:*.part=00;90:*.rej=00;90:*.swp=00;90:*.tmp=00;90:*.dpkg-dist=00;90:*.dpkg-old=00;90:*.ucf-dist=00;90:*.ucf-new=00;90:*.ucf-old=00;90:*.rpmnew=00;90:*.rpmorig=00;90:*.rpmsave=00;90:';
export LS_COLORS

unsetopt BEEP

# Completions
autoload -U compinit
compinit -d ${XDG_CACHE_HOME:-$HOME/.cache}/.zcompdump

# Enable bash completion, requires to source them from somewhere
# autoload -U bashcompinit && bashcompinit

zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
setopt completealiases
zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true # Automatically update cache of binaries avaliable in $PATH
zstyle ':completion:*' insert-tab false
# zstyle ':completion::complete:lsof:*' menu yes select

# Make kill completion smart
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,args -w -w"
# zstyle ':completion:*:*:kill:*:processes' command 'ps xo pid,user:10,cmd | grep -v "sshd:|-zsh$"'

# Colored completion for files and dirs according to LS_COLORS

hash dircolors 2> /dev/null && eval $(dircolors --sh) && \
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Fuzzy completion
# https://superuser.com/questions/415650/does-a-fuzzy-matching-mode-exist-for-the-zsh-shell
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'

zmodload zsh/complist
_comp_options+=(globdots)        # Include hidden files.

## Aliases
alias vi="nvim"
alias wget="wget -c"
alias lsd="ls -lAF | grep --color=never '^d'"
alias df="df -h"
alias psmem="ps aux | sort -nr -k 4 | head -5"
alias pscpu="ps aux | sort -nr -k 3 | head -5"
alias gpg-check="gpg --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg --keyserver-options auto-key-retrieve --receive-keys"
alias mergepdf="gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=_merged.pdf"
alias path='echo -e "${PATH//:/\n}"'
alias ips="grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"
alias ll="ls -l"

# Delete, home, end buttons
bindkey  "^[[3~"  delete-char
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line

# Launch $VISUAL or $EDITOR, for emacsclient if there is no server
# avaliable $ALTERNATE_EDITOR will be used.
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

# Do not require sudo for some system commands.
for command in mount umount sv updatedb su ; do
    alias $command="sudo $command"
done; unset command

# Verbosity and common settings.
alias \
      cp='cp -iv' \
      mv='mv -iv' \
      rm='rm -vI' \
      mkdir='mkdir -pv' \
      ffmpeg='ffmpeg -hide_banner'

# Colorize commands when possible.
alias \
      ls='ls -hp --color=auto' \
      ll='ls -lAh --group-directories-first --color=auto' \
      l='ls -lah --group-directories-first --color=auto' \
      grep='grep --color=auto' \
      diff='diff --color=auto'

# Useful aliases.
alias help=run-help
alias try='guix shell man-db coreutils'
alias ka='killall'
alias sdn='sudo shutdown'

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Key-bindings
# bindkey -e will be emacs mode
bindkey -v # VIM Mode
bindkey -v '^?' backward-delete-char
bindkey -s '^o' 'emacs -nw .^M'
# bindkey -s '^f' 'zi^M'
bindkey -s '^f' 'ncdu^M'
bindkey -s '^n' 'nvim $(fzf)'
# bindkey -s '^v' 'nvim\n'
# bindkey -s '^z' 'zi^M'
# bindkey '^S' fzf-history-widget
# bindkey '^[[P' delete-char
bindkey "^p" up-line-or-beginning-search # Up
bindkey "^n" down-line-or-beginning-search # Down
bindkey "^k" up-line-or-beginning-search # Up
bindkey "^j" down-line-or-beginning-search # Down
bindkey -r "^u"
bindkey -r "^d"

if command -v xset &> /dev/null
then
  # set the keyboard repeat rate (usage):
  # xset r rate [delay] [speed] [b=bell [on/off]]
   xset r rate 300 45 b off
fi

# Edit line in default editor (vim, emacs, etc.) with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
    ZSH_THEME="random"
else
    ZSH_THEME=${ZSH_THEME:-"powerlevel10k"}
    if [ -f "$ZDOTDIR/.p10k.zsh" ]; then
    source "$ZDOTDIR/.p10k.zsh"
    fi
fi

if [ -f "$ZSH/oh-my-zsh.sh" ]; then
    . $ZSH/oh-my-zsh.sh
fi

# Display system info
if command -v neofetch &> /dev/null
then
    neofetch
else if command -v guix &> /dev/null
    guix shell neofetch -- neofetch
fi
