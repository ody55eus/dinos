;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos features backup)
  #:use-module (rde features)
  #:use-module (rde features predicates)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (guix gexp)

  #:export (feature-duply))

;;;
;;; duply.
;;;

(define* (feature-duply
          #:key
          (duply (@ (dinos packages backup) duply)))
  "Setup and configure duply related things."
  (ensure-pred file-like? duply)

  (define (get-home-services config)
    (list
     (simple-service
      'duply-add-duply-package
      home-profile-service-type
      (list duply))))

  (feature
   (name 'duply)
   (values `((duply . #t)))
   (home-services-getter get-home-services)))
