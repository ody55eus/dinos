;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023/2024 Jonathan Pieper <jpieper@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos features emacs-xyz)
  #:use-module (rde features)
  #:use-module (rde features emacs)
  #:use-module (dinos features emacs)
  #:use-module (rde features predicates)
  #:use-module (rde serializers elisp)
  #:use-module (gnu services)
  #:use-module (gnu home-services-utils)
  #:use-module (gnu home services)
  #:use-module (gnu home-services wm)
  #:use-module (guix gexp)

  #:export (feature-emacs-dinos-avy
            feature-emacs-dinos-perspective
            feature-emacs-dinos-roam
            feature-emacs-dinos-dired
            feature-emacs-ag
            feature-emacs-doom-modeline
            feature-emacs-dinos-appearance
            feature-emacs-dinos-eshell
            feature-emacs-dinos-evil
            feature-emacs-tldr
            feature-emacs-plantuml
            feature-emacs-dinos-agenda
            feature-emacs-ibuffer
            feature-emacs-python
            feature-dinos-project
            feature-dinos-modus-themes
            feature-emacs-multiple-cursors))

(define* (feature-emacs-dinos-perspective
          #:key
          (emacs-perspective (@ (gnu packages emacs-xyz) emacs-perspective))
          (persp-prefix "TAB"))
  "Setup and configure related things for the de neo keyboard-layout."
  (ensure-pred file-like? emacs-perspective)
  (ensure-pred maybe-string? persp-prefix)

  (define (get-home-services config)
    (define emacs-f-name 'perspective)
    (define dinos-prefix (get-value 'emacs-prefix-key config "SPC"))

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((with-eval-after-load
              'perspective
            (defun jp/create-persp (&optional name)
              (interactive "sNew name: ")
              (persp-switch "*new Persp*")
              (persp-rename name))
            (defun jp/del-this-persp (&optional name)
              (interactive)
              (persp-kill (persp-current-name)))

            (setq persp-autokill-buffer-on-remove 'kill-weak
                  persp-reset-windows-on-nil-window-conf nil
                  persp-nil-hidden t
                  persp-auto-save-fname "autosave"
                  persp-save-dir (concat user-emacs-directory "workspaces/")
                  persp-set-last-persp-for-new-frames t
                  persp-switch-to-added-buffer nil
                  persp-kill-foreign-buffer-behaviour 'kill
                  persp-remove-buffers-from-nil-persp-behaviour nil
                  persp-auto-resume-time -1 ; Don't auto-load on startup
                  persp-mode-prefix-key (kbd ,(string-append dinos-prefix " " persp-prefix))
                  persp-auto-save-opt 1))) ; auto-save on kill
        #:summary "\
Tweaks for de neo keyboard layout"
        #:commentary "\
s-v: vterm"
        #:keywords '(convenience)
        #:elisp-packages (list emacs-perspective)))))

  (feature
   (name 'dinos-perspective)
   (values `((dinos-perspective . ,emacs-perspective)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-roam
          #:key
          (emacs-org-roam (@ (gnu packages emacs-xyz) emacs-org-roam))
          (emacs-citar-org-roam (@ (gnu packages emacs-xyz) emacs-citar-org-roam))
          (emacs-consult-org-roam (@ (gnu packages emacs-xyz) emacs-consult-org-roam))
          (emacs-org-ref (@ (dinos packages emacs-xyz) emacs-org-ref))
          (emacs-org-roam-bibtex (@ (dinos packages emacs-xyz) emacs-org-roam-bibtex))
          (emacs-vulpea (@ (dinos packages emacs-xyz) emacs-vulpea))
          (emacs-hydra (@ (gnu packages emacs-xyz) emacs-hydra))
          (emacs-ox-pandoc (@ (gnu packages emacs-xyz) emacs-ox-pandoc))
          (pandoc (@ (gnu packages haskell-xyz) pandoc))
          (bibtex-file-path "~/Projects/Bibtex")
          (bibtex-completion-library-path '("~/pdf"))
          (bibtex-completion-notes-path "~/notes/References")
          (bibtex-completion-bibliography '("~/Projects/Bibtex/library.bib"))
          (bibtex-completion-notes-template-multiple-files #f)
          (bibtex-completion-display-formats #f)
          (roam-prefix "r"))
  "Setup and configure additional things for org-roam."
  (ensure-pred file-like? emacs-org-roam)
  (ensure-pred file-like? emacs-org-ref)
  (ensure-pred file-like? emacs-consult-org-roam)
  (ensure-pred file-like? emacs-org-roam-bibtex)
  (ensure-pred file-like? emacs-vulpea)
  (ensure-pred file-like? emacs-hydra)
  (ensure-pred file-like? emacs-ox-pandoc)
  (ensure-pred file-like? pandoc)
  (ensure-pred maybe-string? roam-prefix)
  (ensure-pred maybe-string? bibtex-file-path)
  (ensure-pred list-of-strings? bibtex-completion-library-path)
  (ensure-pred maybe-string? bibtex-completion-notes-path)
  (ensure-pred list-of-strings? bibtex-completion-bibliography)
  (ensure-pred maybe-string? bibtex-completion-notes-template-multiple-files)

  (define (get-home-services config)
    (define emacs-f-name 'roam)
    (define emacs-evil (get-value 'emacs-evil config #f))

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'hydra)
           (require 'bibtex)
           (require 'ox-pandoc)
           (require 'org-ref)
           (require 'org-ref-ivy)
           (require 'org-roam-bibtex)
           (require 'org-roam))

          ;; Autoloads
          (autoload 'helm-bibtex "helm-bibtex" "" t)

          ;; Variables
          (setq bibtex-file-path ,bibtex-file-path
                bibtex-completion-bibliography ',bibtex-completion-bibliography
                bibtex-completion-library-path ',bibtex-completion-library-path
                bibtex-completion-notes-path ,bibtex-completion-notes-path
                org-cite-global-bibliography bibtex-completion-bibliography
                citar-bibliography bibtex-completion-bibliography
                citar-library-paths bibtex-completion-library-path)
          ,@(if bibtex-completion-notes-template-multiple-files
                `((setq bibtex-completion-notes-template-multiple-files ,bibtex-completion-notes-template-multiple-files))
                '())
          ,@(if bibtex-completion-display-formats
                `((setq bibtex-completion-display-formats ',bibtex-completion-display-formats))
                '())

          ;; Org Exports
          (with-eval-after-load
              'ox-pandoc
            (setq org-export-backends '(beamer html man md latex odt org texinfo koma-letter)))

          (with-eval-after-load
              'org-roam
            (defun dinos-org-roam-goto-month ()
              (interactive)
              (org-roam-capture- :goto (when (org-roam-node-from-title-or-alias (format-time-string "%Y-%B")) '(4))
                                 :node (org-roam-node-create)
                                 :templates '(("m" "month" plain "\n* Goals\n\n%?* Summary\n\n"
                                               :if-new (file+head "%<%Y-%B>.org"
                                                                  "#+title: %<%Y-%B>\n#+filetags: Project\n")
                                               :unnarrowed t))))

            (defun dinos-org-roam-goto-year ()
              (interactive)
              (org-roam-capture- :goto (when (org-roam-node-from-title-or-alias (format-time-string "%Y")) '(4))
                                 :node (org-roam-node-create)
                                 :templates '(("y" "year" plain "\n* Goals\n\n%?* Summary\n\n"
                                               :if-new (file+head "%<%Y>.org"
                                                                  "#+title: %<%Y>\n#+filetags: Project\n")
                                               :unnarrowed t))))
            ;; Hydra Menu
            (with-eval-after-load
                'hydra
              (defhydra dinos-org-roam-entry-menu (:hint nil)
                "
  ^Dailies^        ^Capture^       ^Jump^
  ^^^^^^^^-------------------------------------------------
  _t_: today       _T_: today       _m_: current month
  _r_: tomorrow    _R_: tomorrow    _e_: current year
  _y_: yesterday   _Y_: yesterday   ^ ^
  _d_: date        ^ ^              ^ ^
  "
                ("t" org-roam-dailies-goto-today)
                ("r" org-roam-dailies-goto-tomorrow)
                ("y" org-roam-dailies-goto-yesterday)
                ("d" org-roam-dailies-goto-date)
                ("T" org-roam-dailies-capture-today)
                ("R" org-roam-dailies-capture-tomorrow)
                ("Y" org-roam-dailies-capture-yesterday)
                ("m" dinos-org-roam-goto-month)
                ("e" dinos-org-roam-goto-year)
                ("c" nil "cancel")))
            (let ((map mode-specific-map))
              (define-key map (kbd "n a") 'org-roam-alias-add)
              (define-key map (kbd "n A") 'org-roam-alias-remove)
              (define-key map (kbd "n e") 'dinos-org-roam-jump-menu/body)
              (define-key map (kbd "n b") 'helm-bibtex)
              (define-key map (kbd "n s") 'helm-do-grep-ag)
              (define-key map (kbd "n S") 'org-roam-db-sync)
              (define-key map (kbd "n t") 'org-roam-tag-add)
              (define-key map (kbd "n T") 'org-roam-tag-remove)
              (define-key map (kbd "n R") 'org-roam-ref-add)
              (define-key map (kbd "n c") 'orb-citar-edit-note))))
        #:summary "\
Tweaks for org-roam, including org-ref and org-roam-bibtex."
        #:commentary "\
tbd"
        #:keywords '(convenience org-mode org-roam notes)
        #:elisp-packages (append
                          (list emacs-org-roam
                                emacs-org-ref
                                emacs-org-roam-bibtex
                                emacs-citar-org-roam
                                emacs-consult-org-roam
                                emacs-vulpea
                                emacs-ox-pandoc
                                emacs-hydra
                                pandoc)
                          (if emacs-evil
                              (list emacs-evil)
                              '()))))))
  (feature
   (name 'emacs-dinos-roam)
   (values `((emacs-dinos-roam . ,emacs-org-roam)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-agenda
          #:key
          (clockreport-parameter-plist '(:link t
                                         :lang "de"
                                         :formular '%
                                         :narrow nil
                                         :filetitle t
                                         :hidefiles t
                                         :maxlevel 3))
          (duration-format '((special . h:mm)))
          (org-agenda-custom-commands #f))
  "Setup and configure related things for calendar and agenda."
  (ensure-pred maybe-list? org-agenda-custom-commands)
  (ensure-pred maybe-list? duration-format)
  (ensure-pred maybe-list? clockreport-parameter-plist)

  (define (get-home-services config)
    (define emacs-f-name 'agenda)
    (define emacs-org-roam (get-value 'emacs-dinos-roam config
                                      (@ (gnu packages emacs-xyz) emacs-org-roam)))

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `(;; Helper Functions
          (eval-when-compile
           (require 'org-roam))
          (defun dinos/org-roam-visit (node &optional other-window &key templates)
            (if (org-roam-node-file node)
                (org-roam-node-visit node other-window)
                (org-roam-capture-
                 :node node
                 :templates templates
                 :props '(:finalize find-file))))

          (defun dinos/org-roam-select-prefix (prefix)
            (org-roam-node-read
             nil
             (lambda (node)
               (string-prefix-p
                (concat org-roam-directory prefix)
                (org-roam-node-file node)))))

          (defun dinos/org-roam-ignore-prefix (prefix)
            (org-roam-node-read
             nil
             (lambda (node)
               (not (string-prefix-p
                     (concat org-roam-directory prefix)
                     (org-roam-node-file node))))))

          (defun dinos/org-roam-get-tagged (&optional tag)
            (mapcar
             'org-roam-node-file
             (seq-filter
              (lambda (node)
                (and
                 (member tag-name (org-roam-node-tags node))
                 (eq (org-roam-node-level node) 0)))
              (org-roam-node-list))))

          (defun dinos/org-roam-filter-by-tag (tag-name)
            (lambda (node)
              (and
               (member tag-name (org-roam-node-tags node))
               (eq (org-roam-node-level node) 0))))

          (defun dinos/org-roam-list-notes-by-tag (tag-name)
            (mapcar 'org-roam-node-file
                    (seq-filter
                     (dinos/org-roam-filter-by-tag tag-name)
                     (org-roam-node-list))))

          ;; Agenda
          (setq calendar-week-start-day 1  ;; Monday
                org-agenda-start-with-log-mode t
                org-agenda-log-mode-items '(closed clock status)
                org-clock-mode-line-total 'today
                org-habit-graph-column 60
                org-fontify-whole-heading-line t
                org-duration-format ',duration-format
                org-agenda-clockreport-parameter-plist ',clockreport-parameter-plist))
        #:summary "\
Tweaks for org-agenda"
        #:commentary "\
"
        #:keywords '(convenience org-mode calendar agenda)
        #:elisp-packages (list emacs-org-roam)))))

  (feature
   (name 'dinos-agenda)
   (values `((dinos-agenda . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-dired
          #:key
          (emacs-dired-git-info (@ (gnu packages emacs-xyz) emacs-dired-git-info))
          (emacs-dired-rsync (@ (gnu packages emacs-xyz) emacs-dired-rsync)))
  "Setup and configure related things for the de neo keyboard-layout."

  (define (get-home-services config)
    (define emacs-f-name 'dinos-dired)
    (define emacs-evil (get-value 'emacs-evil config #f))

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((defun dinos/dired-hide-dotfiles()
            (interactive)
            (setq dired-omit-files
                  (rx (or (seq bol (? ".") "#")
                          (seq bol "." eol)
                          (seq bol ".." eol)))))
          (with-eval-after-load
              'dired
            (setq dired-recursive-copies  'always
                  dired-recursive-deletes 'top
                  ;; Ask whether destination dirs should get created when copying/removing files.
                  dired-create-destination-dirs 'ask)
            (add-hook 'dired-mode-hook
                      (lambda ()
                        (define-keymap
                          :keymap dired-mode-map
                          "C-c C-r" 'dired-rsync
                          "M-RET" 'dired-display-file
                          "H" 'dinos/dired-hide-dotfiles
                          "J" 'dired-goto-file
                          "Y" 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
                          ")" 'dired-git-info-mode)))))
        #:summary "\
Tweaks for emacs dired and evil"
        #:commentary "\
s-v: vterm"
        #:keywords '(convenience dired)
        #:elisp-packages (list emacs-dired-git-info
                               emacs-dired-rsync)))))

  (feature
   (name 'dinos-dired)
   (values (make-feature-values emacs-dired-git-info))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-avy
          #:key
          (emacs-avy (@ (gnu packages emacs-xyz) emacs-avy)))
  "Setup and configure avy with emacs."

  (define (get-home-services config)
    (define emacs-f-name 'avy)

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'avy))

          ;; Define Avy Actions
          (with-eval-after-load
              'avy
            (defun avy-action-mark-to-char (pt)
              (activate-mark)
              (goto-char pt))
            (defun avy-action-copy-whole-line (pt)
              (save-excursion
               (goto-char pt)
               (cl-destructuring-bind (start . end)
                                      (bounds-of-thing-at-point 'line)
                                      (copy-region-as-kill start end)))
              (select-window
               (cdr
                (ring-ref avy-ring 0)))
              t)
            (defun avy-action-yank-whole-line (pt)
              (avy-action-copy-whole-line pt)
              (save-excursion (yank))
              t)
            (defun avy-action-kill-whole-line (pt)
              (save-excursion
               (goto-char pt)
               (kill-whole-line))
              (select-window
               (cdr
                (ring-ref avy-ring 0)))
              t)
            (defun avy-action-teleport-whole-line (pt)
              (avy-action-kill-whole-line pt)
              (save-excursion (yank)) t)
            (defun dictionary-search-dwim (&optional arg)
              "Search for definition of word at point. If region is active,
search for contents of region instead. If called with a prefix
argument, query for word to search."
              (interactive "P")
              (if arg
                  (dictionary-search nil)
                  (if (use-region-p)
                      (dictionary-search (buffer-substring-no-properties
                                          (region-beginning)
                                          (region-end)))
                      (if (thing-at-point 'word)
                          (dictionary-lookup-definition)
                          (dictionary-search-dwim '(4))))))
            (defun avy-action-define (pt)
              (save-excursion
               (goto-char pt)
               (dictionary-search-dwim))
              (select-window
               (cdr (ring-ref avy-ring 0)))
              t)
            (defun avy-action-helpful (pt)
              (save-excursion
               (goto-char pt)
               (helpful-at-point))
              (select-window
               (cdr (ring-ref avy-ring 0)))
              t)
            (defun avy-action-embark (pt)
              (save-excursion
               (goto-char pt)
               (embark-act))
              (select-window
               (cdr (ring-ref avy-ring 0)))
              t)

            ;; Set Avy Actions to keys (none is default)
            (setf (alist-get ?  avy-dispatch-alist) 'avy-action-mark-to-char
                  (alist-get ?k avy-dispatch-alist) 'avy-action-kill-stay
                  (alist-get ?K avy-dispatch-alist) 'avy-action-kill-whole-line
                  (alist-get ?w avy-dispatch-alist) 'avy-action-copy
                  (alist-get ?W avy-dispatch-alist) 'avy-action-copy-whole-line
                  (alist-get ?m avy-dispatch-alist) 'avy-action-teleport
                  (alist-get ?M avy-dispatch-alist) 'avy-action-teleport-whole-line
                  (alist-get ?= avy-dispatch-alist) 'avy-action-define
                  (alist-get ?H avy-dispatch-alist) 'avy-action-helpful
                  (alist-get ?x avy-dispatch-alist) 'avy-action-embark
                  (alist-get ?y avy-dispatch-alist) 'avy-action-yank
                  (alist-get ?Y avy-dispatch-alist) 'avy-action-yank-whole-line)

            ,@(if (get-value 'neo-layout config)
                  `((setq avy-keys '(?u ?i ?a ?e ?o ?s ?n ?r ?t ?d))))))
        #:summary "\
Tweaks for emacs avy"
        #:commentary "\
tbd"
        #:keywords '(convenience avy)
        #:elisp-packages (list emacs-avy)))))

  (feature
   (name 'avy)
   (values (make-feature-values emacs-avy))
   (home-services-getter get-home-services)))

(define* (feature-emacs-ag
          #:key
          (emacs-ag (@ (gnu packages emacs-xyz) emacs-ag))
          (ag-program (@ (gnu packages code) the-silver-searcher))
          (ag-options " --color --smart-case --search-zip --noheading --numbers %s -- %s %s")
          (ag-key "/"))
  "Setup and configure the silver searcher, a faster ripgrep alternative."
  (ensure-pred file-like? emacs-ag)
  (ensure-pred file-like? ag-program)
  (ensure-pred maybe-string? ag-key)

  (define (get-home-services config)
    (define emacs-f-name 'ag)

    (list
     (simple-service
      'emacs-ag-packages
      home-profile-service-type
      `(,ag-program))
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((setq helm-grep-ag-command ,(file-append ag-program "/bin/ag " ag-options)))
        #:summary "\
Tweaks for emacs searching with the-silver-searcher"
        #:commentary "\
"
        #:keywords '(convenience ag search)
        #:elisp-packages (list emacs-ag)))))

  (feature
   (name 'emacs-ag)
   (values (make-feature-values emacs-ag))
   (home-services-getter get-home-services)))

(define* (feature-emacs-doom-modeline
          #:key
          (emacs-doom-modeline (@ (gnu packages emacs-xyz) emacs-doom-modeline))
          (emacs-nerd-icons (@ (gnu packages emacs-xyz) emacs-nerd-icons)))
  "Setup and configure doom modeline for emacs."
  (ensure-pred file-like? emacs-doom-modeline)
  (define emacs-f-name 'doom-modeline)
  (define f-name (symbol-append 'emacs- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'doom-modeline))
          (with-eval-after-load 'doom-modeline
            (add-hook
             'emacs-startup-hook
             (lambda ()
               (doom-modeline-mode 1)))
            (setq doom-modeline-height 14
                  doom-modeline-support-imenu t
                  doom-modeline-bar-width 4
                  doom-modeline-lsp t
                  doom-modeline-display-default-persp-name t
                  doom-modeline-major-mode-icon t
                  doom-modeline-major-mode-color-icon t
                  doom-modeline-minor-modes nil
                  doom-modeline-modal-icon t)
            (doom-modeline-def-modeline 'dinos
                                        '(bar matches buffer-info remote-host buffer-position parrot selection-info)
                                        '(misc-info minor-modes checker input-method buffer-encoding major-mode process vcs "  "))))
        #:summary "\
Adding doom modeline to emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience doom modeline)
        #:elisp-packages (list emacs-doom-modeline
                               emacs-nerd-icons)))))

  (feature
   (name f-name)
   (values `((,f-name . ,emacs-doom-modeline)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-appearance
          #:key
          (emacs-rainbow-mode (@ (gnu packages emacs-xyz) emacs-rainbow-mode))
          (emacs-hl-todo (@ (gnu packages emacs-xyz) emacs-hl-todo))
          (emacs-ef-themes (@ (gnu packages emacs-xyz) emacs-ef-themes))
          (enable-rainbow-mode? #t)
          (enable-hl-todo? #t)
          (enable-hl-line? #t)
          (enable-relative-lines? #t))
  "Setup and configure packages for better appearance."
  (ensure-pred file-like? emacs-rainbow-mode)
  (ensure-pred file-like? emacs-hl-todo)
  (ensure-pred file-like? emacs-ef-themes)
  (ensure-pred boolean? enable-rainbow-mode?)
  (ensure-pred boolean? enable-hl-todo?)
  (ensure-pred boolean? enable-hl-line?)
  (ensure-pred boolean? enable-relative-lines?)
  (define emacs-f-name 'appearance)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'ef-themes)
           ,@(if enable-rainbow-mode?
                 '((require 'rainbow-mode))
                 '())
           ,@(if enable-hl-todo?
                 '((require 'hl-todo))
                 '()))
          ,@(if enable-rainbow-mode?
                '((dolist (mode '(org-mode-hook
                                  elisp-mode-hook
                                  scheme-mode-hook
                                  sh-mode-hook
                                  c-mode-hook
                                  rst-mode-hook
                                  python-mode-hook))
                          (add-hook mode 'rainbow-mode)))
                '())
          ,@(if enable-hl-line?
                `((with-eval-after-load
                      'hl-line
                    (add-hook 'emacs-startup-hook (lambda () (global-hl-line-mode 1))))))
          ,@(if enable-hl-todo?
                `((with-eval-after-load 'hl-todo
                    (add-hook 'emacs-startup-hook (lambda () (global-hl-todo-mode 1)))
                    (setq hl-todo-keyword-faces
                          '(("TODO"   . "#cc00cc")    ;; TODO
                            ("FIXME"  . "#990000")    ;; FIXME
                            ("NOTE"   . "#009999")    ;; NOTE
                            ("REVIEW" . "#990099")    ;; REVIEW
                            ("DEBUG"  . "#A020F0")    ;; DEBUG
                            ("HACK"   . "#ff6600")    ;; HACK
                            ("GOTCHA" . "#FF4500")    ;; GOTCHA
                            ("STUB"   . "#1E90FF"))))) ;; STUB
                '())

          ,@(if enable-relative-lines?
                `(
                  ;; Disable Line Numbers for specific modes
                  (dolist (mode '(org-mode-hook
                                  term-mode-hook
                                  shell-mode-hook
                                  eshell-mode-hook))
                          (add-hook mode (lambda () (setq display-line-numbers nil))))
                  ;; Enable Line Numbers for programming modes
                  (dolist (mode '(elisp-mode-hook
                                  python-mode-hook
                                  scheme-mode-hook
                                  sh-mode-hook))
                          (add-hook mode (lambda () (setq display-line-numbers 'relative)))))
                '()))
        #:summary "\
Adding better appearances to emacs"
        #:commentary "\
Adding rainbow-mode, hl-todo, hl-line and ef-themes."
        #:keywords '(convenience appearance)
        #:elisp-packages (append
                          (if enable-rainbow-mode?
                              (list emacs-rainbow-mode)
                              '())
                          (if enable-hl-todo?
                              (list emacs-hl-todo)
                              '())
                          (list emacs-ef-themes))))))

  (feature
   (name f-name)
   (values `((,f-name . ,emacs-ef-themes)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-eshell)
  "Setup and configure eshell for emacs."
  (define emacs-f-name 'eshell)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((with-eval-after-load
              'eshell
            (defun dinos-configure-eshell ()
              ;; Save command history when commands are entered
              (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

              ;; Truncate buffer for performance
              (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

              (with-eval-after-load
                  'evil
                ;; Bind some useful keys for evil-mode
                (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
                (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
                (evil-normalize-keymaps))

              (setq eshell-history-size         10000
                    eshell-buffer-maximum-lines 10000
                    eshell-hist-ignoredups t
                    eshell-scroll-to-bottom-on-input t))
            (add-hook 'eshell-first-time-mode-hook 'dinos-configure-eshell)
            (with-eval-after-load 'esh-opt
              (setq eshell-destroy-buffer-when-process-dies t)
              (setq eshell-visual-commands '("htop" "zsh" "vim" "vi" "nvim")))))
        #:summary "\
eshell to emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience eshell)
        #:elisp-packages '()))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-dinos-evil
          #:key
          (emacs-evil-paredit (@ (gnu packages emacs-xyz) emacs-evil-paredit))
          (emacs-evil-lispy (@ (dinos packages emacs-xyz) emacs-evil-lispy))
          (emacs-evil-indent-plus (@ (gnu packages emacs-xyz) emacs-evil-indent-plus))
          (emacs-evil-visual-replace (@ (gnu packages emacs-xyz) emacs-evil-visual-replace))
          (evil-default-cursor '("#339966" box))
          (evil-normal-state-cursor '("#339966" box))
          (evil-replace-state-cursor '("#FF3333" box))
          (evil-insert-state-cursor '("#FFFFFF" (bar . 2)))
          (evil-visual-state-cursor '("#677691" hollow))
          (evil-emacs-state-cursor '("#ebcb8b" box))
          (evil-operator-state-cursor '("#649bce" hollow))
          (evil-motion-state-cursor '("#ad8beb" box)))
  "Setup and configure evil-mode for emacs."
  (ensure-pred file-like? emacs-evil-paredit)
  (ensure-pred file-like? emacs-evil-lispy)
  (ensure-pred file-like? emacs-evil-indent-plus)
  (ensure-pred file-like? emacs-evil-visual-replace)
  (ensure-pred list? evil-default-cursor)
  (ensure-pred list? evil-normal-state-cursor)

  (define emacs-f-name 'evil)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((defun dinos-evil-hook ()
            (dolist (mode '(info-mode))
                    (add-to-list 'evil-emacs-state-modes mode)))
          (with-eval-after-load
              'evil
            (add-hook 'evil-mode-hook 'dinos-evil-hook)
            (global-set-key (kbd "<escape>") 'evil-force-normal-state)
            (setq evil-want-C-w-in-emacs-state t   ; prefixes windows commands in Emacs state
                  evil-want-C-u-scroll t           ; C-u scrolls up (like Vim)
                  evil-want-C-d-scroll t           ; C-d scrolls down (like Vim)
                  evil-split-window-below t
                  evil-vsplit-window-right t
                  evil-default-cursor ',evil-default-cursor
                  evil-normal-state-cursor ',evil-normal-state-cursor
                  evil-replace-state-cursor ',evil-replace-state-cursor
                  evil-insert-state-cursor ',evil-insert-state-cursor
                  evil-visual-state-cursor ',evil-visual-state-cursor
                  evil-emacs-state-cursor ',evil-emacs-state-cursor
                  evil-operator-state-cursor ',evil-operator-state-cursor
                  evil-motion-state-cursor ',evil-motion-state-cursor)))
        #:summary "\
evil for emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience evil)
        #:elisp-packages (list emacs-evil-lispy
                               emacs-evil-indent-plus
                               emacs-evil-visual-replace
                               emacs-evil-paredit)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-tldr
          #:key
          (emacs-tldr (@ (gnu packages emacs-xyz) emacs-tldr))
          (python-tldr (@ (gnu packages python-xyz) python-tldr)))
  "Too long; didn't read (tldr) for emacs."
  (ensure-pred file-like? emacs-tldr)
  (ensure-pred file-like? python-tldr)
  (define emacs-f-name 'tldr)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (simple-service
      'python-tldr-package
      home-profile-service-type
      (list python-tldr))
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'tldr))
          (with-eval-after-load
              'tldr
            (message "tldr configured!")))
        #:summary "\
tldr to emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience help)
        #:elisp-packages (list emacs-tldr)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-plantuml
          #:key
          (emacs-plantuml-mode (@ (gnu packages emacs-xyz) emacs-plantuml-mode))
          (tree-sitter-plantuml (@ (gnu packages tree-sitter) tree-sitter-plantuml))
          (plantuml (@ (gnu packages uml) plantuml))
          (java (@ (gnu packages java) icedtea)))
  "Plantuml for emacs"
  (ensure-pred file-like? emacs-plantuml-mode)
  (ensure-pred file-like? tree-sitter-plantuml)
  (ensure-pred file-like? plantuml)
  (ensure-pred file-like? java)
  (define emacs-f-name 'plantuml)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (simple-service
      'plantuml-packages
      home-profile-service-type
      (list plantuml
            java
            tree-sitter-plantuml))
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((add-hook
           'emacs-startup-hook
           (lambda ()
             (setq org-plantuml-jar-path ,(file-append plantuml "/share/java/plantuml.jar"))
             (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
             (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))))
          (with-eval-after-load 'org
            (add-to-list 'org-structure-template-alist
                         '("pu" . "src plantuml"))))
        #:summary "\
plantuml-mode for emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience help)
        #:elisp-packages (list emacs-plantuml-mode)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-ibuffer
          #:key
          (emacs-ibuffer-vc (@ (gnu packages emacs-xyz) emacs-ibuffer-vc))
          (emacs-all-the-icons-ibuffer (@ (gnu packages emacs-xyz) emacs-all-the-icons-ibuffer)))
  "ibuffer vc and icons for emacs"
  (ensure-pred file-like? emacs-ibuffer-vc)
  (ensure-pred file-like? emacs-all-the-icons-ibuffer)
  (define emacs-f-name 'ibuffer)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((add-hook 'ibuffer-hook
                    (lambda ()
                      (progn
                        (all-the-icons-ibuffer-mode 1)
                        (ibuffer-vc-set-filter-groups-by-vc-root)
                        (unless (eq ibuffer-sorting-mode 'alphabetic)
                          (ibuffer-do-sort-by-alphabetic))))))
        #:summary "\
ibuffer vc and icons for emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience help)
        #:elisp-packages (list emacs-ibuffer-vc
                               emacs-all-the-icons-ibuffer)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-python
          #:key
          (python (@ (gnu packages python) python-3.12))
          (emacs-jedi (@ (gnu packages emacs-xyz) emacs-jedi))
          (tree-sitter-python (@ (gnu packages tree-sitter) tree-sitter-python))
          (python-lsp-server (@ (gnu packages python-xyz) python-lsp-server))
          (python-lsp-server-plugins '()))
  "Python LSP for emacs"
  (ensure-pred file-like? emacs-jedi)
  (ensure-pred file-like? tree-sitter-python)
  (ensure-pred file-like? python-lsp-server)
  (define emacs-f-name 'python)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (simple-service
      'python-lsp-packages
      home-profile-service-type
      (append (list python
                    tree-sitter-python
                    python-lsp-server)
              python-lsp-server-plugins))
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((add-hook 'python-mode-hook
                    (lambda ()
                      (eglot-ensure)
                      (flyspell-prog-mode)))
          (with-eval-after-load 'eglot
            (setq-default
             eglot-workspace-configuration
             (cons '(:pylsp
                     . (:configurationSources ["flake8"]
                        :plugins (:pycodestyle (:enabled :json-false)
                                  :mccabe (:enabled :json-false)
                                  :pyflakes (:enabled :json-false)
                                  :flake8 (:enabled :json-false
                                           :maxLineLength 88)
                                  :pydocstyle (:enabled t
                                               :convention "numpy")
                                  :yapf (:enabled :json-false)
                                  :autopep8 (:enabled :json-false)
                                  :black (:enabled t
                                          :line_length 88
                                          :cache_config t))))
                   eglot-workspace-configuration)))

          (eval-when-compile
           (require 'subr-x))

          (defun rde-find-manifest-file (directory)
            "Recursively search for manifest.scm file in DIRECTORY parents."
            (let ((manifest-file (expand-file-name "manifest.scm" directory))
                  (parent-dir (file-name-directory
                               (directory-file-name directory))))
              (cond
               ((file-exists-p manifest-file) manifest-file)
               ((string= directory parent-dir) nil)
               (t (rde-find-manifest-file parent-dir)))))

          (defun rde-initialize-guix-python-environment ()
            (interactive)
            "Initialize the Python interpreter using Guix shell."
            (let ((manifest-file (rde-find-manifest-file default-directory)))
              (when manifest-file
                (let* ((python-executable
                        (string-trim (shell-command-to-string (concat "\
guix shell --container -m " manifest-file " -- which python3"))))
                       (python-found
                        (and python-executable
                             (not (string-empty-p python-executable))
                             (file-executable-p python-executable)))
                       (site-packages-paths
                        (when python-found
                          (directory-files-recursively
                           (file-name-directory
                            (directory-file-name
                             (file-name-directory python-executable)))
                           "^site-packages$"
                           t))))

                  (when python-found
                    (setq-local python-shell-interpreter python-executable)
                    (setq-local python-shell-interpreter-args
                                (concat
                                 "-i -c \"import sys; "
                                 (mapconcat
                                  (lambda (path)
                                    (format "sys.path.append('%s')" path))
                                  site-packages-paths
                                  "; ")
                                 "\"")))))))

          (add-hook 'python-mode-hook 'rde-initialize-guix-python-environment)
          (add-hook 'python-ts-mode-hook 'rde-initialize-guix-python-environment)

          ,@(if (get-value 'emacs-org config #f)
                `((with-eval-after-load 'org
                    (add-to-list 'org-structure-template-alist
                                 '("py" . "src python")))
                  (with-eval-after-load 'ob-core
                    (require 'ob-python))
                  (with-eval-after-load 'ob-python
                    (setq org-babel-python-command
                          ,(file-append python "/bin/python"))))
                '()))
        #:summary "\
Python LSP for emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience help)
        #:elisp-packages (list emacs-jedi)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-dinos-project
          #:key
          (project-extra-dominating-files
           '(".project.el" ".dir-locals.el" ".gitignore")))
  "Configure project.el, a library to perform operations
on the current project."
  (ensure-pred list? project-extra-dominating-files)

  (define emacs-f-name 'project)
  (define f-name (symbol-append 'emacs- emacs-f-name))

  (define (get-home-services config)
    (list
     (dinos-elisp-configuration-service
      emacs-f-name
      config
      ;; TODO: https://github.com/muffinmad/emacs-ibuffer-project
      ;; MAYBE: Rework the binding approach
      `((eval-when-compile
         (require 'project)
         (require 'cl-lib))
        (defgroup rde-project nil
          "Custom `project.el' enhancements."
          :group 'rde)
        (defcustom rde-project-dominating-files '()
          "List of root files that indicate a directory is a project."
          :group 'rde-project
          :type '(repeat string))

        (cl-defmethod project-root ((project (head explicit)))
          "Determine the PROJECT root."
          (cdr project))

        (defun rde-project-custom-root (dir)
          "Search in project's DIR for a set of project dominating files."
          (let* ((files rde-project-dominating-files)
                 (root (cl-find-if (lambda (file)
                                     (locate-dominating-file dir file))
                                   files)))
            (when root
              (cons 'explicit (locate-dominating-file dir root)))))

        (defun rde-project-org-capture ()
          "Run `org-capture' in the current project root."
          (interactive)
          (when-let ((default-dir (project-root (project-current t))))
            (dir-locals-read-from-dir default-dir)
            (org-capture)))

        (defun rde-project-compile (&optional comint)
          "Compile current project and choose if buffer will be in COMINT mode."
          (interactive "P")
          (let ((default-directory (project-root (project-current t)))
                (compilation-buffer-name-function
                 (or project-compilation-buffer-name-function
                     compilation-buffer-name-function)))
            (call-interactively 'compile nil (and comint (vector (list 4))))))

        (setq rde-project-dominating-files ',project-extra-dominating-files)
        (add-hook 'project-find-functions 'rde-project-custom-root)
        (add-hook 'project-find-functions 'project-try-vc)
        (advice-add 'project-compile :override 'rde-project-compile)

        (define-key global-map (kbd "s-p") project-prefix-map)

        (with-eval-after-load 'project
          (require 'xdg)
          (setq project-switch-use-entire-map t)
          ,@(if (get-value 'emacs-consult config #f)
                '((eval-when-compile
                   (require 'consult))
                  (with-eval-after-load 'consult
                    (define-key project-prefix-map "F" 'consult-find)
                    (define-key project-prefix-map "R" 'consult-ripgrep)
                    (setq consult-project-root-function
                          (lambda ()
                            (when-let (project (project-current))
                                      (car (project-roots project)))))))
                '())
          (setq project-list-file
                (expand-file-name "emacs/projects"
                                  (xdg-cache-home)))

          (defun rde-compilation-buffer-name (mode)
            "Returns the result of `project-prefixed-buffer-name' if inside
project and `compilation--default-buffer-name' if not."
            (if (project-current)
                (project-prefixed-buffer-name mode)
                (compilation--default-buffer-name mode)))

          (setq compilation-buffer-name-function
                'rde-compilation-buffer-name)
          (setq project-compilation-buffer-name-function
                'rde-compilation-buffer-name)

          ;; Project compile and some other things will not work on project
          ;; switch anyway, because default-directory is not yet set.  Also,
          ;; it's one more additional step, which is quite inconvinient.
          ;; (setq project-switch-commands 'project-dired)
          ))
      #:summary "\
Enchancements for project management with project.el"
      #:commentary "\
Keybinding for `project-prefix-map', integration with consult and minor
adjustments."
      #:keywords '(convenience project)
      #:elisp-packages (list (get-value 'emacs-consult config (@ (gnu packages emacs-xyz)
                                                                 emacs-consult))))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))

(define* (feature-dinos-modus-themes
          #:key
          (emacs-modus-themes (@ (gnu packages emacs-xyz) emacs-modus-themes))
          (extra-after-enable-theme-hooks '())
          (dark? #f)
          (deuteranopia? #t)
          (deuteranopia-red-blue-diffs? #f)
          (headings-scaling? #f)
          (extra-modus-themes-overrides '()))
  "Configure modus-themes, a set of elegant and highly accessible
themes for Emacs.  DEUTERANOPIA? replaces red/green tones with yellow/blue,
which helps people with color blindness.  If DEUTERANOPIA-RED-BLUE-DIFFS?  is
set, red/blue colors will be used instead.  If HEADINGS-SCALING? is set,
different level headings will have different size."
  (ensure-pred file-like? emacs-modus-themes)
  (ensure-pred list? extra-after-enable-theme-hooks)
  (ensure-pred boolean? dark?)
  (ensure-pred boolean? deuteranopia?)
  (ensure-pred boolean? headings-scaling?)
  (ensure-pred elisp-config? extra-modus-themes-overrides)

  (define emacs-f-name 'modus-themes)
  (define f-name (symbol-append 'emacs- emacs-f-name))
  (define dark-theme
    (if deuteranopia? 'modus-vivendi-deuteranopia 'modus-vivendi))
  (define light-theme
    (if deuteranopia? 'modus-operandi-deuteranopia 'modus-operandi))

  (define (get-home-services config)
    "Return home services related to modus-themes."
    (define mode-line-padding (get-value 'emacs-mode-line-padding config))
    (define header-line-padding (get-value 'emacs-header-line-padding config))
    (define tab-bar-padding (get-value 'emacs-tab-bar-padding config))
    (define theme
      (if dark? dark-theme light-theme))

    (list
     (rde-elisp-configuration-service
      emacs-f-name
      config
      `((eval-when-compile
          (require 'modus-themes)
          (require 'cl-seq))
        (require ',(symbol-append theme '-theme))
        (eval-when-compile
         (enable-theme ',theme))
        (defgroup rde-modus-themes nil
          "Configuration related to `modus-themes'."
          :group 'rde)
        (defcustom rde-modus-themes-mode-line-padding 1
          "The padding of the mode line."
          :type 'number
          :group 'rde-modus-themes)
        (defcustom rde-modus-themes-tab-bar-padding 1
          "The padding of the tab bar."
          :type 'number
          :group 'rde-modus-themes)
        (defcustom rde-modus-themes-header-line-padding 1
          "The padding of the header line."
          :type 'number
          :group 'rde-modus-themes)
        (defcustom rde-modus-themes-after-enable-theme-hook nil
          "Normal hook run after enabling a theme."
          :type 'hook
          :group 'rde-modus-themes)

        (defun rde-modus-themes-run-after-enable-theme-hook (&rest _args)
          "Run `rde-modus-themes-after-enable-theme-hook'."
          (run-hooks 'rde-modus-themes-after-enable-theme-hook))

        (defun rde-modus-themes-set-custom-faces (&optional _theme)
          "Set faces based on the current theme."
          (interactive)
          (when (modus-themes--current-theme)
            (modus-themes-with-colors
              (custom-set-faces
               `(window-divider ((,c :foreground ,bg-main)))
               `(window-divider-first-pixel ((,c :foreground ,bg-main)))
               `(window-divider-last-pixel ((,c :foreground ,bg-main)))
               `(vertical-border ((,c :foreground ,bg-main)))
               `(tab-bar
                 ((,c :background ,bg-dim
                      :box (:line-width ,rde-modus-themes-tab-bar-padding
                            :color ,bg-dim))))
               `(mode-line
                 ((,c :box (:line-width ,rde-modus-themes-mode-line-padding
                            :color ,bg-mode-line-active))))
               `(mode-line-inactive
                 ((,c :box (:line-width ,rde-modus-themes-mode-line-padding
                            :color ,bg-mode-line-inactive))))
               `(header-line
                 ((,c :box (:line-width ,rde-modus-themes-header-line-padding
                            :color ,bg-dim))))
               `(git-gutter-fr:added ((,c :foreground ,bg-added-fringe
                                          :background ,bg-main)))
               `(git-gutter-fr:deleted ((,c :foreground ,bg-removed-fringe
                                            :background ,bg-main)))
               `(git-gutter-fr:modified ((,c :foreground ,bg-changed-fringe
                                             :background ,bg-main)))
               `(aw-leading-char-face ((,c :height 1.0
                                           :foreground ,blue-cooler)))))))

        (defun rde-modus-themes--dark-theme-p (&optional theme)
          "Indicate if there is a curently-active dark THEME."
          (if theme
              (eq theme ',light-theme)
              (eq (car custom-enabled-themes) ',dark-theme)))

        (setq rde-modus-themes-header-line-padding ,header-line-padding)
        (setq rde-modus-themes-tab-bar-padding ,tab-bar-padding)
        (setq rde-modus-themes-mode-line-padding ,mode-line-padding)
        (advice-add 'enable-theme
                    :after 'rde-modus-themes-run-after-enable-theme-hook)
        ,@(map (lambda (hook)
                 `(add-hook 'rde-modus-themes-after-enable-theme-hook ',hook))
               (append
                '(rde-modus-themes-set-custom-faces)
                 extra-after-enable-theme-hooks))

        (with-eval-after-load 'rde-keymaps
          (define-key rde-toggle-map (kbd "t") 'modus-themes-toggle))

        (with-eval-after-load 'modus-themes
          (setq modus-themes-common-palette-overrides
                '((border-mode-line-active unspecified)
                  (border-mode-line-inactive unspecified)
                  (fringe unspecified)
                  (fg-line-number-inactive "gray50")
                  (fg-line-number-active fg-main)
                  (bg-line-number-inactive unspecified)
                  (bg-line-number-active unspecified)
                  (bg-region bg-ochre)
                  (fg-region unspecified)
                  ,@extra-modus-themes-overrides))
          ,@(if deuteranopia-red-blue-diffs?
                `((setq modus-operandi-deuteranopia-palette-overrides
                        '((bg-changed         "#ffdfa9")
                          (bg-changed-faint   "#ffefbf")
                          (bg-changed-refine  "#fac090")
                          (bg-changed-fringe  "#d7c20a")
                          (fg-changed         "#553d00")
                          (fg-changed-intense "#655000")

                          (bg-removed         "#ffd8d5")
                          (bg-removed-faint   "#ffe9e9")
                          (bg-removed-refine  "#f3b5af")
                          (bg-removed-fringe  "#d84a4f")
                          (fg-removed         "#8f1313")
                          (fg-removed-intense "#aa2222")))

                  (setq modus-vivendi-deuteranopia-palette-overrides
                        '((bg-changed         "#363300")
                          (bg-changed-faint   "#2a1f00")
                          (bg-changed-refine  "#4a4a00")
                          (bg-changed-fringe  "#8a7a00")
                          (fg-changed         "#efef80")
                          (fg-changed-intense "#c0b05f")

                          (bg-removed         "#4f1119")
                          (bg-removed-faint   "#380a0f")
                          (bg-removed-refine  "#781a1f")
                          (bg-removed-fringe  "#b81a1f")
                          (fg-removed         "#ffbfbf")
                          (fg-removed-intense "#ff9095"))))
                '())
          (setq modus-themes-to-toggle '(,light-theme ,dark-theme))
          (setq modus-themes-italic-constructs t)
          (setq modus-themes-bold-constructs t)
          (setq modus-themes-mixed-fonts t)
          (setq modus-themes-org-blocks 'gray-background)
          ,@(if headings-scaling?
                `((setq modus-themes-headings (quote ((1 . (1.15))
                                                      (2 . (1.1))
                                                      (3 . (1.1))
                                                      (4 . (1.0))
                                                      (5 . (1.0))
                                                      (6 . (1.0))
                                                      (7 . (0.9))
                                                      (8 . (0.9))))))
                '()))
        (load-theme ',theme t (not (display-graphic-p)))
        ,@(if (get-value 'emacs-server-mode? config #f)
              `((add-hook 'server-after-make-frame-hook
                             (lambda ()
                               (enable-theme ',theme))))
              '()))
      #:elisp-packages (list emacs-modus-themes)
      #:summary "Modus Themes extensions"
      #:commentary "Customizations to Modus Themes, the elegant,
highly legible Emacs themes.\

Modus operandi is light, high-contrast, calm, colorblind-friendly.
The light colorschemes are better for productivity according to
various researchs, more eye-friendly and works better with other apps
and media like PDFs, web pages, etc, which are also light by default.
Later here will be a link to rde manual with more in-depth explanation
with references to researches.")))

  (feature
   (name f-name)
   (values `((,f-name . ,emacs-modus-themes)
             (emacs-light-theme . ,light-theme)
             (emacs-dark-theme . ,dark-theme)))
   (home-services-getter get-home-services)))

(define* (feature-emacs-multiple-cursors
          #:key
          (emacs-multiple-cursors (@ (gnu packages emacs-xyz) emacs-multiple-cursors))
          (emacs-phi-search (@ (gnu packages emacs-xyz) emacs-phi-search))
          (emacs-phi-search-mc (@ (gnu packages emacs-xyz) emacs-phi-search-mc)))
  "multiple cursors for emacs"
  (ensure-pred file-like? emacs-multiple-cursors)
  (ensure-pred file-like? emacs-phi-search)
  (ensure-pred file-like? emacs-phi-search-mc)
  (define emacs-f-name 'multiple-cursors)
  (define f-name (symbol-append 'dinos- emacs-f-name))

  (define (get-home-services config)
    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((eval-when-compile
           (require 'multiple-cursors)
           (require 'phi-search))

          (with-eval-after-load 'multiple-cursors
            (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
            (global-set-key (kbd "C->") 'mc/mark-next-like-this)
            (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
            (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this))

          (with-eval-after-load 'phi-search
            (with-eval-after-load 'phi-search-mc
              (phi-search-mc/setup-keys))

            (global-set-key (kbd "C-s") 'phi-search)
            (global-set-key (kbd "C-r") 'phi-search-backward)))
        #:summary "\
multiple cursors for emacs"
        #:commentary "\
tbd"
        #:keywords '(convenience help)
        #:elisp-packages (list emacs-multiple-cursors
                               emacs-phi-search
                               emacs-phi-search-mc)))))

  (feature
   (name f-name)
   (values `((,f-name . #t)))
   (home-services-getter get-home-services)))
