;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos features emacs)
  #:use-module (rde features)
  #:use-module (rde features predicates)
  #:use-module (rde features emacs)
  #:use-module (gnu home services)
  #:use-module (rde home services emacs)
  #:use-module (gnu home-services wm)
  #:use-module (gnu home services xdg)
  #:use-module (gnu services)

  #:use-module (guix gexp)
  #:use-module (rde gexp)
  #:use-module (guix packages)
  #:use-module (guix transformations)

  #:export (dinos-elisp-configuration-service
            feature-emacs-sane-defaults))

(define* (dinos-emacs-configuration-package
          name
          #:optional (elisp-expressions '())
          #:key
          summary authors url keywords commentary
          (elisp-packages '())
          (autoloads? #f))
  "Returns a package, which configures emacs.  Can be used as a
dependency for other packages."
  (let* ((configure-package
          (elisp-configuration-package
           (string-append "dinos-" (symbol->string name))
           elisp-expressions
           #:elisp-packages elisp-packages
           #:autoloads? autoloads?
           #:summary summary
           #:commentary commentary
           #:keywords keywords
           #:url (or url "https://dinos.ody5.de")
           #:authors (or authors '("Jonathan Pieper <ody55eus@mailbox.org>")))))
    configure-package))

(define* (dinos-elisp-configuration-service
          name config
          #:optional (elisp-expressions '())
          #:key
          summary authors url keywords commentary
          (early-init '())
          (elisp-packages '()))
  "Adds a configure-NAME package to the profile and emacs load path and if
emacs-portable? rde value is present adds autoloads cookies to each expression
of it, otherwise adds a require to @file{init.el}."
  (service
   (make-home-elisp-service-type (symbol-append 'emacs-dinos- name))
   (home-elisp-configuration
    (name (symbol-append 'dinos- name))
    ;; TODO: Rename the field to elisp-expressions for cosistency?
    (config elisp-expressions)
    (early-init early-init)
    (elisp-packages elisp-packages)
    (authors (or authors '("Jonathan Pieper <ody55eus@mailbox.org>")))
    (url (or url "https://dinos.ody5.de"))
    (summary summary)
    (commentary commentary)
    (keywords (or keywords '())))))

(define* (feature-emacs-sane-defaults
          #:key
          (fill-column 80)
          (longitude #f)
          (latitude #f))
  "Setup and configure related things for the de neo keyboard-layout."
  (ensure-pred maybe-integer? fill-column)


  (define (get-home-services config)
    (define emacs-f-name 'defaults)

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `(;; Frame Transparency
          (defun jp/toggle-window-transparency ()
            "Toggle transparency."
            (interactive)
            (let ((alpha-transparency 85))
              (if (eq (frame-parameter nil 'alpha-background) alpha-transparency)
                  (set-frame-parameter nil 'alpha-background 100)
                  (set-frame-parameter nil 'alpha-background alpha-transparency))))
          (with-eval-after-load
              'rde-keymaps
            (define-key rde-toggle-map (kbd "a") 'jp/toggle-window-transparency))

          (setq-default delete-by-moving-to-trash t
                        mouse-yank-at-point t
                        cursor-type 'box
                        window-combination-resize t)
          ,@(if (get-value 'gpg-primary-key config)
                `((setq epg-user-id ,(get-value 'gpg-primary-key config)))
                '())
          ,@(if (and longitude latitude)
                `((setq calendar-longitude ,longitude
                        calendar-latitude  ,latitude))
                '())
          (setq python-executable ,(file-append (@ (gnu packages python) python) "/bin/python3")
                find-program ,(file-append (@ (gnu packages base) findutils) "/bin/find")
                tab-width 2
                undo-limit 8000000
                indent-tabs-mode nil
                scroll-margin 2
                x-stretch-cursor t
                trash-directory (concat (getenv "HOME") "/.Trash")
                create-lockfiles nil
                ;; make-backup-files nil
                backup-by-copying t
                version-control t
                vc-make-backup-files t
                delete-old-versions 0
                use-short-answers t
                fill-column ,fill-column
                locale-coding-system 'utf-8
                blink-cursor-interval 0.6
                which-key-idle-delay 0.3
                auth-sources '((:source "~/.authinfo.gpg"))
                auto-save-default nil))
        #:summary "\
Default emacs settings"
        #:commentary "\
sane configuration"
        #:keywords '(convenience)
        #:elisp-packages '()))))

  (feature
   (name 'defaults)
   (values '())
   (home-services-getter get-home-services)))
