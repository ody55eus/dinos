;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos features finance)
  #:use-module (rde features)
  #:use-module (dinos features emacs)
  #:use-module (rde features predicates)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (guix gexp)

  #:export (feature-beancount
            feature-beancount-fava))

;;;
;;; beancount.
;;;

(define* (feature-beancount
          #:key
          (beancount (@ (gnu packages finance) beancount))
          (emacs-beancount (@ (gnu packages finance) emacs-beancount)))
  "Setup and configure beancount related things."
  (ensure-pred file-like? beancount)
  (ensure-pred file-like? emacs-beancount)

  (define (get-home-services config)
    (define emacs-f-name 'beancount)
    (define emacs-cmd (get-value 'emacs-client-create-frame config))

    (list
     (simple-service
      'beancount-add-beancount-package
      home-profile-service-type
      (list beancount))

     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((with-eval-after-load
           'beancount-mode
           (setq beancount-default-date-format ledger-iso-date-format)))
        #:summary "\
Tweaks for beancount"
        #:commentary "\
Use ISO date."
        #:keywords '(convenience)
        #:elisp-packages (list emacs-beancount)))))

  (feature
   (name 'beancount)
   (values `((beancount . ,beancount)
             (emacs-beancount . ,emacs-beancount)))
   (home-services-getter get-home-services)))

(define* (feature-beancount-fava
          #:key
          (fava (@ (dinos packages python-xyz) python-fava))
          (fava-extra-packages (list (@ (dinos packages python-xyz) python-fava-envelope))))
  "Setup and configure fava for beancount."
  (ensure-pred file-like? fava)
  (ensure-pred list-of-file-likes? fava-extra-packages)

  (define (get-home-services config)
    (list
     (simple-service
      'beancount-add-fava-package
      home-profile-service-type
      (append
       (list fava)
       fava-extra-packages))))

  (feature
   (name 'beancount-fava)
   (values `((beancount-fava . ,fava)))
   (home-services-getter get-home-services)))
