;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos features keyboard)
  #:use-module (rde features)
  #:use-module (dinos features emacs)
  #:use-module (rde features predicates)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (gnu home-services wm)
  #:use-module (guix gexp)

  #:export (feature-neo))

;;;
;;; neo.
;;;

(define emacs-evil (@ (gnu packages emacs-xyz) emacs-evil))
(define emacs-all-the-icons-ibuffer (@ (gnu packages emacs-xyz) emacs-all-the-icons-ibuffer))
(define default-emacs-neo-packages (list
                                    emacs-evil
                                    emacs-all-the-icons-ibuffer))

(define* (feature-neo
          #:key
          (prefix-key "SPC")
          (meta-key "M")
          (ctrl-key "C")
          (super-key "s")
          (additional-packages default-emacs-neo-packages)
          (custom-term-key "v")            ;; Emacs Vterm Launch Key (e.g. Super + 'v')
          (application-launcher-key "r"))  ;; Sway App Launcher Key (e.g. Super + 'r')
  "Setup and configure related things for the de neo keyboard-layout."
  (ensure-pred maybe-string? custom-term-key)
  (ensure-pred maybe-string? application-launcher-key)
  (ensure-pred maybe-string? prefix-key)
  (ensure-pred maybe-string? meta-key)
  (ensure-pred maybe-string? ctrl-key)
  (ensure-pred maybe-string? super-key)

  (define (get-home-services config)
    (define emacs-f-name 'neo)

    (list
     (when (get-value 'emacs config)
       (dinos-elisp-configuration-service
        emacs-f-name
        config
        `((defun sudo-file-path (file)
            (let ((host (or (file-remote-p file 'host) "localhost")))
              (concat "/" (when (file-remote-p file)
                            (concat (file-remote-p file 'method) ":"
                                    (if-let (user (file-remote-p file 'user))
                                            (concat user "@" host)
                                            host)
                                    "|"))
                      "sudo:root@" host
                      ":" (or (file-remote-p file 'localname)
                              file))))

          (defun jp-sudo-find-file (file)
            (interactive "Open file as root: ")
            (find-file (sudo-file-path file)))

          (defun jp-sudo-this-file ()
            (interactive)
            (find-file
             (sudo-file-path
              (or buffer-file-name
                  (when (or (derived-mode-p 'dired-mode)
                            (derived-mode-p 'wdired-mode))
                    default-directory)))))

          (defun jp-sudo-save-buffer ()
            "Save this file as root."
            (interactive)
            (let ((file (sudo-file-path buffer-file-name)))
              (if-let (buffer (find-file-noselect file))
                      (let ((origin (current-buffer)))
                        (copy-to-buffer buffer (point-min) (point-max))
                        (unwind-protect
                         (with-current-buffer buffer
                                              (save-buffer))
                         (unless (eq origin buffer)
                           (kill-buffer buffer))
                         (with-current-buffer origin
                                              (revert-buffer t t))))
                      (user-error "Unable to open %S" file))))

          (defun +default/yank-buffer-contents ()
            "Copy entire buffer into kill ring."
            (interactive)
            (clipboard-kill-ring-save (point-min) (point-max)))

          (defun +default/yank-buffer-path (&optional root)
            "Copy the current buffer's path to the kill ring."
            (interactive)
            (if-let (filename (or (buffer-file-name (buffer-base-buffer))
                                  (bound-and-true-p list-buffers-directory)))
                    (let ((path (abbreviate-file-name
                                 (if root
                                     (file-relative-name filename root)
                                     filename))))
                      (kill-new path)
                      (if (string= path (car kill-ring))
                          (message "Copied path: %s" path)
                          (user-error "Couldn't copy filename in current buffer")))
                    (error "Couldn't find filename in current buffer")))

          (defun +default/insert-file-path (arg)
            "Insert the file name (absolute path if prefix ARG).
If `buffer-file-name' isn't set, uses `default-directory'."
            (interactive "P")
            (let ((path (or buffer-file-name default-directory)))
              (insert
               (if arg
                   (abbreviate-file-name path)
                   (file-name-nondirectory path)))))

          (with-eval-after-load
              'rde-keymaps
            (with-eval-after-load
                'keymap

              (define-key global-map (kbd ,(string-append "s-" custom-term-key)) 'vterm)
              (define-key rde-app-map (kbd ,custom-term-key) 'vterm)

              ;; (global-set-key
              ;;  (kbd ,(string-append meta-key "-" prefix-key))
              ;; (define-key global-map (kbd "C-ä") ')
              ;; (define-key global-map (kbd "C-ö") ')
              )))
        #:summary "\
Tweaks for de neo keyboard layout"
        #:commentary "\
s-v: vterm"
        #:keywords '(convenience)
        #:elisp-packages additional-packages))
     (when (get-value 'sway config)
       (simple-service
        'sway-neo
        (@ (rde home services wm) home-sway-service-type)
        `((bindsym ,(string-append "$mod+" application-launcher-key) exec $menu))))))

  (feature
   (name 'emacs-neo)
   (values (append
            (make-feature-values emacs-all-the-icons-ibuffer)
            `((neo-layout . #t)
              (emacs-prefix-key . ,prefix-key))))
   (home-services-getter get-home-services)))
