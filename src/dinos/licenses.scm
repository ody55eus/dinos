(define-module (dinos licenses)
  #:use-module (srfi srfi-9)
  #:use-module (guix licenses)
  #:export (cc-by-nc-nd4.0))

(define license (@@ (guix licenses) license))

(define cc-by-nc-nd4.0
  (license "CC-BY-NC-ND 4.0"
           "http://creativecommons.org/licenses/by-nc-nd/4.0/"
           "Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International"))
