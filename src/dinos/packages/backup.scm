(define-module (dinos packages backup)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages ftp)
  #:use-module (gnu packages time)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages check)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages openstack)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web))

(define-public duply
  (package
    (name "duply")
    (version "2.5.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://sourceforge/ftplicity/duply%20%28simple%20duplicity%29/"
             (version-major+minor version) ".x/duply_"
             (version-major+minor+point version) ".tgz"))
       (sha256
        (base32 "0iyyx60nyxq0z6hvl4szxx64wg548fi3n3s5vrac0ai128pclpxk"))))
    (build-system copy-build-system)
    (inputs (list bash-minimal)) ; to run the wrapped program
    (propagated-inputs (list duplicity-3 python))
    (arguments
     (list
      #:install-plan #~'(("duply" "bin/duply")
                         ("gpl-2.0.txt" #$(string-append "share/doc/" name "-"
                                                         version "/LICENSE.txt"))
                         ("CHANGELOG.txt" #$(string-append "share/doc/" name "-"
                                                           version "/CHANGELOG.txt")))
      #:phases #~(modify-phases %standard-phases
                   (add-after 'install 'wrap-executable
                     (lambda* (#:key inputs outputs #:allow-other-keys)
                       (let ((out (assoc-ref outputs "out"))
                             (python (string-append (assoc-ref inputs "python")
                                                    "/bin/python3")))
                         (wrap-program (string-append out "/bin/duply")
                           `("DUPL_PYTHON_BIN" =
                             (,python)))))))))
    (home-page "https://duply.net/Duply_(simple_duplicity)")
    (synopsis "Wrapper for automated duplicity backup configuration")
    (description
     "duply simplifies running duplicity with cron or on command line by:

- keeping recurring settings in profiles per backup job

- automated import/export of keys between profile and keyring

- enabling batch operations eg. backup_verify_purge

- executing pre/post scripts

- precondition checking for flawless duplicity operation")
    (license license:gpl2)))

(define-public duplicity-3
  (package
    (inherit duplicity)
    (name "duplicity")
    (version "3.0.2")
    (source
     (origin
      (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/duplicity/duplicity")
             (commit (string-append "rel." version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "00ggx5d2lzyn8wy1f0zbz1yn3iby8nawcjirlb2bd0m6vv98k3m9"))))
    (build-system pyproject-build-system)
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               (add-before 'build 'use-store-file-names
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute* "duplicity/gpginterface.py"
                     (("self.call = u'gpg'")
                      (string-append "self.call = '"
                                     (search-input-file inputs
                                                        "/bin/gpg")
                                     "'")))
                   (substitute* "duplicity/backends/giobackend.py"
                     (("subprocess.Popen\\(\\[u'dbus-launch'\\]")
                      (string-append "subprocess.Popen([u'"
                                     (search-input-file inputs
                                                        "/bin/dbus-launch") "']")))
                   (substitute* '("testing/functional/__init__.py"
                                  "testing/overrides/bin/lftp")
                     (("/bin/sh")
                      (which "sh")))))
               (add-before 'build 'fix-version
                 (lambda _
                   (substitute* "duplicity/__init__.py"
                     (("\\$version")
                      #$(package-version this-package)))))
               (add-before 'check 'set-up-tests
                 (lambda* (#:key inputs #:allow-other-keys)
                   (setenv "HOME"
                           (getcwd)) ; gpg needs to write to $HOME
                   (setenv "TZDIR" ; some timestamp checks need TZDIR
                           (search-input-directory inputs
                                                   "share/zoneinfo"))
                   ;; Some things respect TMPDIR, others hard-code /tmp, and the
                   ;; defaults don't match up, breaking test_restart.  Fix it.
                   (setenv "TMPDIR" "/tmp")))
               (delete 'sanity-check) ;; Needs to install missing propagated inputs first
               (add-after 'wrap 'gi-wrap
                 (lambda _
                   (let ((prog (string-append #$output "/bin/duplicity")))
                     (wrap-program prog
                       `("GI_TYPELIB_PATH" = (,(getenv "GI_TYPELIB_PATH"))))))))))
    (native-inputs
     (list gettext-minimal ; for msgfmt
           gobject-introspection
           util-linux ; setsid command, for the tests
           par2cmdline
           python-fasteners
           python-future ; for tests
           python-paramiko
           python-pexpect
           python-pytest
           python-pytest-runner
           python-setuptools-scm
           tzdata-for-tests
           python-mock))
    (inputs
     (list bash-minimal ; to run the wrapped program
           dbus ; dbus-launch (Gio backend)
           librsync
           lftp
           gnupg ; gpg executable needed
           util-linux))     ; for setsid
    (propagated-inputs
     (list python-oauthlib python-requests-oauthlib python-keystoneclient
           python-swiftclient python-google-auth-oauthlib
           python-google-api-client python-azure-storage-blob
           python-paramiko python-psutil python-httplib2
           python-boto3 python-botocore python-chardet
           python-cryptography python-dropbox python-fasteners python-keyring
           python-lxml python-pexpect python-atom python-megatools python-mediafire
           python-gettext
           ;;python-pyrax ;; python-jottalib
           ;;python-pydrive2 ;; python-gdata
           python-b2sdk python-boxsdk))
    (home-page "https://duplicity.gitlab.io/duplicity-web/")
    (synopsis "Encrypted backup using rsync algorithm")
    (description
     "Duplicity backs up directories by producing encrypted tar-format volumes
and uploading them to a remote or local file server.  Because duplicity uses
librsync, the incremental archives are space efficient and only record the
parts of files that have changed since the last backup.  Because duplicity
uses GnuPG to encrypt and/or sign these archives, they will be safe from
spying and/or modification by the server.")
    (license license:gpl2+)))

(define-public python-gettext
  (package
    (name "python-gettext")
    (version "5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-gettext" version))
       (sha256
        (base32 "0k08qmszwc94j6pjcqi8w401hbx6q8j9h9ap0lcbdnp38pmg36l6"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools
                         python-wheel))
    (home-page "https://github.com/hannosch/python-gettext")
    (synopsis "Python Gettext po to mo file compiler.")
    (description "Python Gettext po to mo file compiler.")
    (license license:bsd-3)))

(define-public python-b2sdk
  (package
    (name "python-b2sdk")
    (version "2.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "b2sdk" version))
       (sha256
        (base32 "1qmrbcr9af548ds68x5hw32irqlmij3cvj3i2dxrliw936lg6vk2"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f
       #:phases (modify-phases %standard-phases
                  (delete 'sanity-check))))
    (native-inputs (list python-pdm-backend))
    (propagated-inputs (list python-typing-extensions
                             python-requests
                             ;;python-logfury
                             ))
    (home-page "")
    (synopsis "Backblaze B2 SDK")
    (description "Backblaze B2 SDK.")
    (license license:expat)))

(define-public python-logfury
  (package
    (name "python-logfury")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "logfury" version))
       (sha256
        (base32 "09kanw1iv61hgmd858xkc9kck8ia91qdyb954i4m7bdrxan5s2hk"))))
    (build-system python-build-system)
    (native-inputs (list python-setuptools-scm))
    (home-page "https://github.com/reef-technologies/logfury")
    (synopsis
     "('Toolkit for responsible, low-boilerplate logging of library method calls',)")
    (description
     "('Toolkit for responsible, low-boilerplate logging of library method calls',).")
    (license license:bsd-3)))

(define-public python-boxsdk
  (package
    (name "python-boxsdk")
    (version "3.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "boxsdk" version))
       (sha256
        (base32 "1lj665v778m1sh8183pa8gzy56w9szlfkxk4ga9afjnjn2ym04y0"))))
    (build-system pyproject-build-system)
    (arguments
     (list #:tests? #f))
    (propagated-inputs (list python-attrs python-dateutil python-requests
                             python-requests-toolbelt python-urllib3
                             python-pyjwt))
    (native-inputs (list python-bottle
                         python-setuptools
                         python-wheel
                         python-jsonpatch
                         python-pytest
                         python-pytest-cov
                         python-pytest-lazy-fixtures
                         python-pytest-timeout
                         python-pytz
                         python-sqlalchemy
                         python-tox
                         python-urllib3))
    (home-page "https://github.com/box/box-python-sdk")
    (synopsis "Official Box Python SDK")
    (description "Official Box Python SDK.")
    (license #f)))

(define-public python-atom
  (package
    (name "python-atom")
    (version "0.11.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "atom" version))
       (sha256
        (base32 "1q5fy8bi13i53b1xp6mlkgmi6aax4gvfj5miaf10fskvrk1kzfaa"))))
    (build-system pyproject-build-system)
    (arguments
     (list #:tests? #f))
    (inputs (list python-cppy nss-certs))
    (native-inputs (list python-setuptools
                         python-setuptools-scm
                         python-cppy
                         python-wheel))
    (propagated-inputs (list python-typing-extensions))
    (home-page "")
    (synopsis "Memory efficient Python objects")
    (description "Memory efficient Python objects.")
    (license #f)))

(define-public python-megatools
  (package
    (name "python-megatools")
    (version "0.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "megatools" version))
       (sha256
        (base32 "04h1cd2h6r24sxaw2clvh717pm2p2wx1aaifscbr8jzcsrzvc624"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools
                         python-wheel))
    (home-page "https://github.com/Harkame/Megatools")
    (synopsis "Megatools's wrapper")
    (description "Megatools's wrapper.")
    (license #f)))

(define-public python-mediafire
  (package
    (name "python-mediafire")
    (version "0.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mediafire" version))
       (sha256
        (base32 "0gimg7l5rr4ihnq96l5inabm2nx033ngc869c3ai3dnz8gzzxbd1"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools
                         python-wheel))
    (propagated-inputs (list python-requests python-requests-toolbelt
                             python-six))
    (home-page "")
    (synopsis "Python MediaFire client library")
    (description "Python @code{MediaFire} client library.")
    (license license:bsd-3)))

(define-public python-pyrax
  (package
    (name "python-pyrax")
    (version "1.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyrax" version))
       (sha256
        (base32 "1ck7wgjrdkrfyy4ks2k9blx5rhpaxv6mjgmyrfz3qdnzqydh76qh"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-keyring
                             python-mock
                             ;;python-novaclient
                             ;;python-rackspace-novaclient
                             python-requests
                             python-six))
    (home-page "https://github.com/pycontribs/pyrax")
    (synopsis "Python language bindings for OpenStack Clouds.")
    (description "Python language bindings for @code{OpenStack} Clouds.")
    (license license:asl2.0)))

(define-public python-jottalib
  (package
    (name "python-jottalib")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "jottalib" version))
       (sha256
        (base32 "19dh5dk2r4slp3wahkrhc33aqhdd415zz2i7jva2lsphf8brlp01"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-certifi
                             python-chardet
                             python-clint
                             python-humanize
                             python-lxml
                             python-dateutil
                             python-requests
                             python-requests-toolbelt
                             python-six))
    (home-page "https://github.com/havardgulldahl/jottalib")
    (synopsis "A library and tools to access the JottaCloud API")
    (description
     "This package provides a library and tools to access the @code{JottaCloud} API.")
    (license #f)))

(define-public python-pydrive2
  (package
    (name "python-pydrive2")
    (version "1.21.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pydrive2" version))
       (sha256
        (base32 "1q6xd6h194d3x62h9fzi96203ay638jjx8r0hvi24scsl9205nkh"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-google-api-client
                             python-oauth2client python-pyopenssl
                             python-pyyaml))
    (native-inputs (list python-black
                         python-flake8
                         python-flake8-docstrings
                         python-funcy
                         python-importlib-resources
                         python-pyinstaller
                         python-pytest
                         python-pytest-mock
                         python-timeout-decorator))
    (home-page "https://github.com/iterative/PyDrive2")
    (synopsis "Google Drive API made easy. Maintained fork of PyDrive.")
    (description
     "Google Drive API made easy.  Maintained fork of @code{PyDrive}.")
    (license #f)))

(define-public python-pyinstaller
  (package
    (name "python-pyinstaller")
    (version "6.11.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyinstaller" version))
       (sha256
        (base32 "1bv4k0fpki9dhbfnf79w966ph2pq9nxzfb2zrwbrs3dk7lx46kfb"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-altgraph python-packaging
                             python-pyinstaller-hooks-contrib
                             python-setuptools))
    (native-inputs (list python-execnet python-psutil python-pytest))
    (home-page "https://www.pyinstaller.org/")
    (synopsis
     "PyInstaller bundles a Python application and all its dependencies into a single package.")
    (description
     "@code{PyInstaller} bundles a Python application and all its dependencies into a
single package.")
    (license #f)))

(define-public python-pyinstaller-hooks-contrib
  (package
    (name "python-pyinstaller-hooks-contrib")
    (version "2024.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyinstaller_hooks_contrib" version))
       (sha256
        (base32 "1380zgmhbc1xbaxjg3bx8q1yghz3j0lgs7hhdj0c878d6ygqd4s7"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-packaging python-setuptools
                             python-towncrier))
    (home-page "https://github.com/pyinstaller/pyinstaller-hooks-contrib")
    (synopsis "Community maintained hooks for PyInstaller")
    (description "Community maintained hooks for @code{PyInstaller}.")
    (license #f)))

(define-public python-flake8-docstrings
  (package
    (name "python-flake8-docstrings")
    (version "1.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "flake8_docstrings" version))
       (sha256
        (base32 "1bs1m5kqw25sn68f06571q5s3aaxd06mv7k952bqdrhnvi4cg32c"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flake8 python-pydocstyle))
    (home-page "https://github.com/pycqa/flake8-docstrings")
    (synopsis "Extension for flake8 which uses pydocstyle to check docstrings")
    (description
     "Extension for flake8 which uses pydocstyle to check docstrings.")
    (license license:expat)))
