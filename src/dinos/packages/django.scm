(define-module (dinos packages django)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix deprecation)
  #:use-module (guix search-paths)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml))


(define-public python-django-5
  (package
    (name "python-django")
    (version "5.1.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "Django" version))
              (sha256
               (base32
                "1w7lrpmhhb5gpxgganf7gpywj1l795vfdvi26xj6pycr1kwpcwxx"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         ;; XXX: The 'wrap' phase adds native inputs as runtime dependencies,
         ;; see <https://bugs.gnu.org/25235>.  The django-admin script typically
         ;; runs in an environment that has Django and its dependencies on
         ;; PYTHONPATH, so just disable the wrapper to reduce the size from
         ;; ~710 MiB to ~203 MiB.
         (delete 'wrap))))
    ;; TODO: Install extras/django_bash_completion.
    (native-inputs
     (list tzdata-for-tests
           ;; Remaining packages are test requirements taken from
           ;; tests/requirements/py3.txt
           python-docutils
           ;; optional for tests: python-geoip2
           ;; optional for tests: python-memcached
           python-numpy
           python-pillow
           python-pyyaml
           ;; optional for tests: python-selenium
           python-tblib))
    (propagated-inputs
     (list python-asgiref-3.8
           python-sqlparse
           ;; Optional dependencies.
           python-argon2-cffi
           python-bcrypt
           ;; This input is not strictly required, but in practice many Django
           ;; libraries need it for test suites and similar.
           python-jinja2))
    (native-search-paths
     ;; Set TZDIR when 'tzdata' is available so that timezone functionality
     ;; works (mostly) out of the box in containerized environments.
     ;; Note: This search path actually belongs to 'glibc'.
     (list $TZDIR))
    (home-page "https://www.djangoproject.com/")
    (synopsis "High-level Python Web framework")
    (description
     "Django is a high-level Python Web framework that encourages rapid
development and clean, pragmatic design.  It provides many tools for building
any Web site.  Django focuses on automating as much as possible and adhering
to the @dfn{don't repeat yourself} (DRY) principle.")
    (license license:bsd-3)
    (properties `((cpe-name . "django")))))

(define-public python-asgiref-3.8
  (package
    (inherit python-asgiref)
    (version "3.8.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "asgiref" version))
              (sha256
               (base32 "146mhkn3zp2bqf7j6r3chdlvfzgs5x1lrnqahsllgjdyl20bshy3"))))))
