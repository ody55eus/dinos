;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2022,2023 Jonathan Pieper <ody55eus@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos packages emacs-doom)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages finance)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages uml)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public emacs-doom
  (let ((commit "042fe0c43831c8575abfdec4196ebd7305fa16ac")
        (revision "7"))
    (package
      (name "emacs-doom")
      (version (git-version "3.0.0" revision commit))
      (home-page "https://github.com/doomemacs/doomemacs")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1z9cjksph1q0v6sz1lhnvdcv1hd5cx7vyzi3fn28ph7q0yxmq62y"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("bin" "bin")
                          ("." "share/doom/"))))
      (propagated-inputs (list binutils))
      (license license:expat)
      (synopsis "An Emacs framework for the stubborn martian hacker.")
      (description
       "Doom is a configuration framework for GNU Emacs tailored for
                   Emacs bankruptcy veterans who want less framework in their
                   frameworks, a modicum of stability (and reproducibility) from
                   their package manager, and the performance of a hand rolled config
                   (or better). It can be a foundation for your own config or a
                   resource for Emacs enthusiasts to learn more about our favorite
                   operating system.
                                Its design is guided by these mantras:
                                Gotta go fast. Startup and run-time performance are priorities. Doom goes beyond by modifying packages to be snappier and load lazier.
                                Close to metal. There's less between you and vanilla Emacs by design. That's less to grok and less to work around when you tinker. Internals ought to be written as if reading them were part of Doom's UX, and it is!
                                Opinionated, but not stubborn. Doom is about reasonable defaults and curated opinions, but use as little or as much of it as you like.
                                Your system, your rules. You know better. At least, Doom hopes so! It won't automatically install system dependencies (and will force plugins not to either). Rely on doom doctor to tell you what's missing.
                        Nix/Guix is a great idea! The Emacs ecosystem is temperamental. Things break and they break often. Disaster recovery should be a priority! Doom's package management should be declarative and your private config reproducible, and comes with a means to roll back releases and updates (still a WIP)."))))

(define-public emacs-doom-org-avy
  (package
    (name "emacs-doom-org-avy")
    (version "1.0.0")
    (home-page "https://github.com/doomemacs/doomemacs")
    (source
      (file-append
       emacs-doom
       "/share/doom/modules/lang/org/autoload/org-avy.el"))
    (build-system emacs-build-system)
    (license license:expat)
    (synopsis "Doom Emacs: Org Avy Autoloads")
    (description "")))
