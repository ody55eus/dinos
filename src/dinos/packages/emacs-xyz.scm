;;; DinOS --- DinOS is not an Operating System.
;;;
;;; Copyright © 2023/2024 Jonathan Pieper <jpieper@mailbox.org>
;;;
;;; This file is part of DinOS.
;;;
;;; DinOS is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; DinOS is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with DinOS.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dinos packages emacs-xyz)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages finance)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages uml)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public emacs-german-holidays
  (let ((commit "a8462dffccaf2b665f2032e646b5370e993a386a")
        (revision "0"))
    (package
      (name "emacs-german-holidays")
      (version (git-version "0.2.1" revision commit))
      (source (origin
                (uri (git-reference
                      (url "https://github.com/rudolfochrist/german-holidays")
                      (commit commit)))
                (method git-fetch)
                (sha256
                 (base32
                  "1rf8p42pl7jmmdiibfcamlbr3kg6kslffv8vbpwn20xm2ii13rxz"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/rudolfochrist/german-holidays")
      (synopsis "Emacs national holidays for Germany.")
      (description
       "German holidays for Emacs calendar.")
      (license license:gpl3+))))

(define-public emacs-vulpea
  (let ((commit "f5c7a68b5308336927d24a166681a2a1903289c3")
        (revision "0"))
    (package
      (name "emacs-vulpea")
      (version (git-version "0.3.0" revision commit))
      (source (origin
                (uri (git-reference
                      (url "https://github.com/d12frosted/vulpea")
                      (commit commit)))
                (method git-fetch)
                (sha256
                 (base32
                  "17pbn74gixvzdrj4yac3kgswid8ranjm8x15f4360604wmyz7ga7"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-org-roam
             emacs-org
             emacs-dash
             emacs-s))
      (home-page "https://github.com/d12frosted/vulpea")
      (synopsis "A collection of functions for note taking based on org-roam.")
      (description
       "Great tool for automating things around Org mode files and interfacing with
        sqlite and org-roam.")
      (license license:gpl3+))))

(define-public emacs-pcache
  (let ((commit "507230d094cc4a5025fe09b62569ad60c71c4226")
        (revision "0"))
    (package
      (name "emacs-pcache")
      (version (git-version "0.5.1" revision commit))
      (source (origin
                (uri (git-reference
                      (url "https://github.com/sigma/pcache")
                      (commit commit)))
                (method git-fetch)
                (sha256
                 (base32
                  "1fjdn4g9ww70f3x6vbzi3gqs9dsmqg16isajlqlflzw2716zf2nh"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sigma/pcache")
      (synopsis "Emacs package for persistent caching")
      (description
       "pcache provides a hashtable-like persistent data cache for Emacs.")
      (license license:gpl2+))))

(define-public emacs-persistent-soft
  (package
    (name "emacs-persistent-soft")
    (version "0.8.10")
    (source (origin
              (uri (git-reference
                    (url "https://github.com/rolandwalker/persistent-soft")
                    (commit (string-append "v" version))))
              (method git-fetch)
              (sha256
               (base32
                "14p20br8vzxs39d4hswzrrkgwql5nnmn5j17cpbabzjvck42rixc"))
              (file-name (git-file-name name version))))
    (build-system emacs-build-system)
    (propagated-inputs `(("emacs-list-utils" ,emacs-list-utils)
                         ("emacs-pcache" ,emacs-pcache)))
    (home-page "https://github.com/rolandwalker/persistent-soft")
    (synopsis "Emacs package for persistent data storage")
    (description
     "persistent-soft is an Emacs package which wraps pcache.el to provide
\"soft\" fetch and store routines which return `nil' instead of throwing an
error when a key is not found.")
    (license license:bsd-2)))

(define-public emacs-unicode-fonts
  (let ((commit "44d0a22420c39709d1e1fa659a3f135facf3c986")
        (revision "1"))
    (package
      (name "emacs-unicode-fonts")
      (version (git-version "0.4.10" revision commit))
      (source (origin
                (uri (git-reference
                      (url "https://github.com/rolandwalker/unicode-fonts")
                      (commit commit)))
                (method git-fetch)
                (sha256
                 (base32
                  "00qdwkphwpc5kddn3k3ck1isykbhlvqmfb45877a65274am79pd7"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs `(("emacs-list-utils" ,emacs-list-utils)
                           ("emacs-persistent-soft" ,emacs-persistent-soft)
                           ("emacs-pcache" ,emacs-pcache)))
      (home-page "https://github.com/rolandwalker/unicode-fonts")
      (synopsis "Emacs package for configuring unicode fonts")
      (description
       "unicode-fonts is a package that enables you to configure Unicode fonts
and configure font mappings correctly from specific Unicode blocks.  This can
be especially helpful for displaying emoji fonts in Emacs.")
      (license license:bsd-2))))

(define-public emacs-evil-lispy
  (let ((commit "ed317f7fccbdbeea8aa04a91b1b1f48a0e2ddc4e")
        (revision "0"))
    (package
      (name "emacs-evil-lispy")
      (version "1.1")
      (source (origin
                (uri (git-reference
                      (url "https://github.com/emacsmirror/evil-lispy")
                      (commit commit)))
                (method git-fetch)
                (sha256
                 (base32
                  "0izgd9zwfwykmznv6wjrq9czmjqc1hkw41szrjmrcxy5kbz1p5c0"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs `(("emacs-evil" ,emacs-evil)
                           ("emacs-hydra" ,emacs-hydra)
                           ("emacs-lispy" ,emacs-lispy)))
      (home-page "https://github.com/sp3ctum/evil-lispy")
      (synopsis "Evil keybindings for Emacs' Lispy package")
      (description
       "evil-lispy provides a layer of Evil Mode friendly bindings for the structural
editing package called Lispy.")
      (license license:gpl3+))))

(define-public emacs-org-ref
  (let ((commit "fd178abf12a85f8e12005d1df683564bdc534124")
        (revision "1"))
    (package
      (name "emacs-org-ref")
      (version (git-version "3.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/jkitchin/org-ref")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0rdi1n4vay93xs7gwxxmivg3lq13sk10f0g505z0hagzcv2f48w7"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:include #~(cons* "org-ref.org" "org-ref.bib" "^citeproc/" %default-include)
        #:exclude #~(list
                     ;; github.com/jkitchin/org-ref/issues/1085
                     ;; "openalex.el"
                     ;; author doesn't recommend using it
                     "org-ref-pdf.el")))
      (propagated-inputs
       (list emacs-avy
             emacs-citeproc-el
             emacs-dash
             emacs-f
             emacs-helm
             emacs-helm-bibtex
             emacs-htmlize
             emacs-hydra
             emacs-ivy
             emacs-request
             emacs-ox-pandoc
             emacs-parsebib
             emacs-s))
      (home-page "https://github.com/jkitchin/org-ref")
      (synopsis "Citations, cross-references and bibliographies in Org mode")
      (description
       "Org Ref is an Emacs library that provides rich support for citations,
labels and cross-references in Org mode.

The basic idea of Org Ref is that it defines a convenient interface to insert
citations from a reference database (e.g., from BibTeX files), and a set of
functional Org links for citations, cross-references and labels that export
properly to LaTeX, and that provide clickable functionality to the user.  Org
Ref interfaces with Helm BibTeX to facilitate citation entry, and it can also
use RefTeX.

It also provides a fairly large number of utilities for finding bad citations,
extracting BibTeX entries from citations in an Org file, and functions to
create and modify BibTeX entries from a variety of sources, most notably from
a DOI.

Org Ref is especially suitable for Org documents destined for LaTeX export and
scientific publication.  Org Ref is also useful for research documents and
notes.")
      (license license:gpl3+))))

(define-public emacs-org-roam-bibtex
  (let ((commit "d9b8a57cfca832e3e7c7f414bf93060acbf63573")
        (revision "0"))
    (package
      (name "emacs-org-roam-bibtex")
      (version (git-version "0.6.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/org-roam/org-roam-bibtex")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1ww421apmn887602b9pddh76sd25x6jq3z1x0vah5zpglh2sqd6z"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-helm-bibtex emacs-org-ref emacs-org-roam))
      (home-page "https://github.com/org-roam/org-roam-bibtex")
      (synopsis "Connector between Org Roam, BibTeX-completion, and Org Ref")
      (description
       "Org Roam BibTeX is a library which offers a tighter integration between
Org Roam, Helm-BibTeX, and Org Ref.  It allows users to access their
bibliographical notes in Org Roam directory via Helm BibTeX, Ivy BibTeX, or by
opening Org Ref's @code{cite:} links.")
      (license license:gpl3+))))
