(define-module (dinos packages fonts)
  #:use-module (ice-9 regex)
  #:use-module ((dinos licenses)
                #:prefix license:)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system font)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system trivial)
  #:use-module ((gnu packages autotools)
                #:select (autoconf automake))
  #:use-module (gnu packages c)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module ((gnu packages tex)
                #:select (texlive-bin))
  #:use-module (gnu packages xorg))

(define-public font-nerd-fonts
  (package
    (name "font-nerd-fonts")
    (version "3.0.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ryanoasis/nerd-fonts")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0jld6wkb34n1lw27112yg3f1xv878inrcia60w8791annhv13kwx"))))
    (build-system font-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-before 'install 'make-files-writable
                    (lambda _
                      (for-each make-file-writable
                                (find-files "." ".*\\.(otf|otc|ttf|ttc)$")) #t)))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "Iconic font aggregator, collection, and patcher")
    (description
     "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
    (license (list license:expat
                   license:asl2.0
                   license:cc-by4.0
                   license:cc-by-sa4.0
                   license:cc-by-nc-nd4.0
                   license:wtfpl2
                   license:silofl1.1))))

(define-public font-nerd-fonts-jetbrains
  (package
    (name "font-nerd-fonts-jetbrains")
    (version "3.0.2")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/ryanoasis/nerd-fonts/releases/download/v"
                    version "/JetBrainsMono.zip"))
              (file-name "JetBrainsMono.zip")
              (sha256
               (base32
                "1kpx71i3sqzzzz59c78bfr787h18ljsywvkrp9yr397wid3rg8qz"))))
    (build-system font-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (replace 'unpack
                    (lambda* (#:key source #:allow-other-keys)
                      (invoke "unzip" source)))
                  (add-before 'install 'make-files-writable
                    (lambda _
                      (for-each make-file-writable
                                (find-files "." ".*\\.(otf|otc|ttf|ttc)$")) #t)))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "Iconic font aggregator, collection, and patcher")
    (description
     "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others.")
    (license (list license:expat
                   license:asl2.0
                   license:cc-by4.0
                   license:cc-by-sa4.0
                   license:cc-by-nc-nd4.0
                   license:wtfpl2
                   license:silofl1.1))))

(define-public font-font-awesome
  (package
    (name "font-font-awesome")
    (version "6.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/FortAwesome/Font-Awesome")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0mmjq8z1sldg8s7cspskbljc88qhyv8djwbxv89lw1ajdydmfk29"))))
    (build-system font-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-before 'install 'make-files-writable
                    (lambda _
                      (for-each make-file-writable
                                (find-files "." ".*\\.(otf|otc|ttf|ttc)$")) #t)))))
    (home-page "https://fontawesome.com")
    (synopsis "The iconic SVG, font, and CSS toolkit")
    (description
     "The internet's most popular icon toolkit has been redesigned and
built from scratch. On top of this, features like icon font ligatures,
an SVG framework, official NPM packages for popular frontend libraries
like React, and access to a new CDN.")
    (license #f)))

(define-public lcdf-typetools
  (package
    (name "lcdf-typetools")
    (version "2.108")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kohler/lcdf-typetools")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a6jqaqwq43ldjjjlnsh6mczs2la9363qav7v9fyrfzkfj8kw9ad"))))
    (inputs `(("texlive-bin" ,texlive-bin)
              ("autoconf" ,autoconf)
              ("automake" ,automake)))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags (list (string-append "--with-kpathsea="
                                              (assoc-ref %build-inputs
                                                         "texlive-bin")))))
    (home-page "http://www.lcdf.org/type/")
    (synopsis "Utilities for manipulating various fonts")
    (description
     "LCDF Typetools comprises several programs for manipulating
PostScript Type 1, Type 1 Multiple Master, OpenType, and TrueType
fonts.")
    (license license:gpl2+)))
