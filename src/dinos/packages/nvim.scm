(define-module (dinos packages nvim)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages python)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages vim)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public neovim-config
  (let ((commit "9c08b359bc0f2e47c32760225e6d2ac14a72be00")
        (revision "3"))
    (package
      (name "neovim-config")
      (version (git-version "git" revision commit))
      (home-page "https://github.com/LunarVim/nvim-basic-ide")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1029jr65k84hykcz4xlwkhgcxr3z7d5w7k7sjrbrfyqzha1y7s4q"))
                (modules '((guix build utils)))
                (snippet
                 '(begin
                    (substitute* "lua/user/alpha.lua"
                      (("chrisatmachine.com") "DinOS"))))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("lua" "share/"))))
      (license license:gpl3)
      (synopsis "")
      (description ""))))


(define-public neovim-nvchad
  (let ((commit "d0c602f5f155d4d1261609219e9b8a61e936d681")
        (revision "1"))
    (package
      (name "neovim-nvchad")
      (version (git-version "git" revision commit))
      (home-page "https://github.com/NvChad/starter")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "08wfkn39360087q6izzdhpfrmc83w35v2bf5can4hpsmp6kmwnj9"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("lua" "share/nvchad/lua")
                          ("init.lua" "share/nvchad/init.lua"))))
      (license license:gpl3)
      (synopsis "")
      (description ""))))

(define-public neovim-lunarvim
  (let ((commit "36c8bdee9ff59a0a63c1edfc445b5eb2886cf246")
        (branch "master")
        (revision "1"))
    (package
      (name "neovim-lunarvim")
      (version (git-version "git" revision commit))
      (home-page "https://github.com/LunarVim/LunarVim")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "184idilx702chshcp0y7a6r7y5gzn0n85ai33ayb71v64fg6vrkl"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan #~'(("lua" "share/lua")
                                ("utils/bin/lvim.template" "bin/lvim.template")
                                ("utils/installer/config.example.lua"
                                 "config/lvim/config.lua"))))
      (propagated-inputs (list neovim python-pynvim git))
      (license license:gpl3)
      (synopsis "")
      (description ""))))
