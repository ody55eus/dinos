(define-module (dinos packages python-xyz)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages finance)
  #:use-module (gnu packages time)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages crates-io)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public python-fava
(package
  (name "python-fava")
  (version "1.29")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "fava" version))
            (sha256
             (base32
              "0zsnljpizlwb9azgn6klsb37wd9alx2yinyi7vm8b1awc7ipb5ji"))))
  (build-system pyproject-build-system)
  (arguments
   '(#:tests? #f
     #:phases (modify-phases %standard-phases
                (delete 'sanity-check))))
  (native-inputs (list python-setuptools
                       python-pep517-bootstrap
                       python-setuptools-scm))
  (propagated-inputs (list python-babel-2.16
                           beancount
                           python-bottle
                           python-beautifulsoup4
                           python-cheroot
                           python-chardet
                           python-click
                           python-dateutil
                           python-flask-3
                           python-flask-babel-4
                           python-jinja2
                           python-magic
                           python-markdown2
                           python-pdfminer-six
                           python-ply
                           python-pytest
                           python-simplejson
                           ;;python-watchfiles  ;; not yet working
                           python-werkzeug-3))
  (home-page "https://beancount.github.io/fava/")
  (synopsis "Web interface for the accounting tool Beancount.")
  (description "Web interface for the accounting tool Beancount.")
  (license license:expat)))

(define-public python-flask-3
  (package
    (inherit python-flask)
    (version "3.0.3")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "Flask" version))
              (sha256
               (base32
                "0hk8yw721km9v75lbi0jhmdif1js2afxk918g5rs4gl2yc57pcnf"))))
    (build-system pyproject-build-system)
    (arguments
     (list #:tests? #f))
    (propagated-inputs
     (list python-asgiref               ;async extra
           python-click
           python-importlib-metadata
           python-itsdangerous-2.2
           python-jinja2
           python-werkzeug-3
           python-blinker-1.8))
    (native-inputs
     (list python-flit-core))))

(define-public python-itsdangerous-2.2
  (package
    (name "python-itsdangerous")
    (version "2.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "itsdangerous" version))
       (sha256
        (base32 "0wv1bwzbfvpsh4ng5gn4d4mjwvjwpg7w0jgiz8zsbvm1gl5hq1g0"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs
     (list python-flit-core))
    (home-page "https://palletsprojects.com/p/itsdangerous/")
    (synopsis "Python library for passing data to/from untrusted environments")
    (description
     "Itsdangerous provides various helpers to pass trusted data to untrusted
environments and back.")
    (license license:bsd-3)))

(define-public python-flask-babel-4
  (package
    (name "python-flask-babel")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "flask_babel" version))
       (sha256
        (base32 "14x508k9lkhgnmxdgg9wg7mr454fx6b68s0ii9kqfjizg81b9snv"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f))
    (propagated-inputs
     (list python-flask-3 python-babel-2.16 python-jinja2 python-pytz))
    (native-inputs
     (list python-poetry-core))
    (home-page "https://github.com/python-babel/flask-babel")
    (synopsis "Adds i18n/l10n support for Flask applications.")
    (description "Adds i18n/l10n support for Flask applications.")
    (license #f)))

;; TODO: check missing build dependencie
(define-public python-watchfiles
  (package
    (name "python-watchfiles")
    (version "0.24.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "watchfiles" version))
       (sha256
        (base32 "1q9gvx36c5gjgs184agvvbz2lz2d9fzbih89q0la99sgnwjj7dxg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-notify" ,rust-notify-6))))
    (native-inputs
     (list rust-cargo
           maturin
           python-anyio))
    (home-page "https://github.com/samuelcolvin/watchfiles")
    (synopsis
     "Simple, modern and high performance file watching and code reload in python.")
    (description
     "Simple, modern and high performance file watching and code reload in python.")
    (license license:expat)))

(define-public python-blinker-1.8
  (package
    (name "python-blinker")
    (version "1.8.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "blinker" version))
       (sha256
        (base32 "10rdcklyr5655fz28x709sfw7sf5q8wnyj79d7lrbizp7ffv0xwg"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs
     (list python-flit-core))
  (home-page "https://pythonhosted.org/blinker/")
    (synopsis "Fast, simple object-to-object and broadcast signaling")
    (description
     "Blinker provides a fast dispatching system that allows any number of
interested parties to subscribe to events, or \"signals\".")
    (license license:expat)))

(define-public python-babel-2.16
  (package
    (name "python-babel")
    (version "2.16.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "babel" version))
       (sha256
        (base32 "05p3k0i5h8v4vqsg36s94kwl4nhgfmgwdq1x7wbzw1b6l965bwyi"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs (list python-freezegun python-pytest python-pytest-cov))
    (home-page "https://babel.pocoo.org/")
    (synopsis "Internationalization utilities")
    (description "Internationalization utilities.")
    (license #f)))

(define-public python-werkzeug-3
  (package
    (name "python-werkzeug")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "werkzeug" version))
       (sha256
        (base32 "1m6s7vyz0n38sfd0x09hy48c4ljfjzi66q8mbyb53q61vzxrvlwc"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (replace 'check
           (lambda* (#:key tests? inputs outputs #:allow-other-keys)
             (when tests?
               (add-installed-pythonpath inputs outputs)
               (invoke "python" "-m" "pytest"
                       ;; Test tries to use the network.
                       "-k not test_reloader_sys_path")))))))
    (propagated-inputs
     (list python-requests))
    (native-inputs
     (list python-pytest python-pytest-timeout python-pytest-xprocess
           python-flit-core-bootstrap
           python-ephemeral-port-reserve
           python-markupsafe
           python-watchdog-6))
    (home-page "https://palletsprojects.com/p/werkzeug/")
    (synopsis "Utilities for WSGI applications")
    (description "One of the most advanced WSGI utility modules.  It includes a
powerful debugger, full-featured request and response objects, HTTP utilities to
handle entity tags, cache control headers, HTTP dates, cookie handling, file
uploads, a powerful URL routing system and a bunch of community-contributed
addon modules.")
    (license license:x11)))

(define-public python-watchdog-6
  (package
    (name "python-watchdog")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "watchdog" version))
       (sha256
        (base32 "10n2v2iflhdriwfp34yvhfcckqb6vs7378fdvqj8xbm3zn17rpwx"))))
    (build-system pyproject-build-system)
   (arguments
    '(#:tests? #f))
    (native-inputs
     (list python-pytest
           python-pytest-cov
           python-pytest-timeout
           python-sphinx
           python-mypy
           python-pyaml
           python-flaky))
    (home-page "https://github.com/gorakhargosh/watchdog")
    (synopsis "Filesystem events monitoring")
    (description "Filesystem events monitoring.")
    (license #f)))

(define-public python-ephemeral-port-reserve
  (package
    (name "python-ephemeral-port-reserve")
    (version "1.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ephemeral_port_reserve" version))
       (sha256
        (base32 "1chl9hil7ggz6l4sfhmp0l2j55qcskbc3pj9360b0309jwndmxxq"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/Yelp/ephemeral-port-reserve/")
    (synopsis
     "Bind to an ephemeral port, force it into the TIME_WAIT state, and unbind it.")
    (description
     "Bind to an ephemeral port, force it into the TIME_WAIT state, and unbind it.")
    (license license:expat)))

(define-public python-cheroot
  (package
   (name "python-cheroot")
   (version "10.0.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "cheroot" version))
            (sha256
             (base32
              "1w0ind0dza9j1py56y23344piqkpyfmcm060qfrnk6gggy3s3i2r"))))
   (build-system python-build-system)
   (arguments
    '(#:tests? #f))
   (native-inputs (list python-setuptools-scm-git-archive))
   (propagated-inputs (list python-jaraco-functools python-six))
   (home-page "https://cheroot.cherrypy.dev")
   (synopsis "Highly-optimized, pure-python HTTP server")
   (description "Highly-optimized, pure-python HTTP server")
   (license license:bsd-3)))

(define-public python-markdown2
  (package
   (name "python-markdown2")
   (version "2.4.13")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "markdown2" version))
            (sha256
             (base32
              "17vs6in5w9w7ygg9hqqxqzqcicqmikj5prc24g1g4xysj1jvbkhq"))))
   (build-system python-build-system)
   (home-page "https://github.com/trentm/python-markdown2")
   (synopsis "A fast and complete Python implementation of Markdown")
   (description
    "This package provides a fast and complete Python implementation of Markdown")
   (license license:expat)))

(define-public python-fava-envelope
  (package
    (name "python-fava-envelope")
    (version "0.5.6")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "fava-envelope" version))
              (sha256
               (base32
                "1727rp5yyra0h26slxsfs3jvc1x12xmvfm130pfk6fvqxxyd1mn2"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f
       #:phases (modify-phases %standard-phases
                  (delete 'sanity-check))))
    (propagated-inputs (list beancount
                             python-fava
                             python-pandas))
    (home-page "https://github.com/polarmutex/fava-envelope")
    (synopsis "A beancount fava extension to add a envelope budgeting capability to fava and beancount.")
    (description "A beancount fava extension to add a envelope budgeting capability to fava and beancount. It is developed as an fava plugin and CLI.")
    (license license:expat)))

(define-public python-docstring-to-markdown
  (package
    (name "python-docstring-to-markdown")
    (version "0.15")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "docstring-to-markdown" version))
              (sha256
               (base32
                "0gdpabnyl1kyy0cjrnph6xl4fyhgim50a1amsaqq3hahki6i2ip1"))))
    (build-system python-build-system)
    (home-page "https://github.com/python-lsp/docstring-to-markdown")
    (synopsis "")
    (description "")
    (license license:lgpl2.1)))

(define-public python-lsp-server-1.11
  (package
    (inherit python-lsp-server)
    (version "1.11.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-lsp-server" version))
       (sha256
        (base32
         "11lf7c9dpf8jzz5y7dllz8l1lka887m9a79xbazy8lkq7zxxdvc9"))))
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-after 'unpack 'set-version
                    (lambda _
                      (substitute* "pyproject.toml"
                        (("dynamic = \\[\"version\"\\]")
                         (string-append "version = \""
                                        ,version "\"")))))
                  (add-before 'check 'set-HOME
                    (lambda _
                      (setenv "HOME" "/tmp")))
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        ;; Disable failing test.
                        (invoke "python" "-m" "pytest" "-k"
                                "not test_pyqt_completion")))))))
    (native-inputs (modify-inputs (package-native-inputs python-lsp-server)
                     (append python-whatthepatch)))
    (propagated-inputs (modify-inputs (package-propagated-inputs python-lsp-server)
                         (delete "python-pydocstyle")
                         (delete "python-rope")
                         (delete "python-lsp-jsonrpc")
                         (prepend
                          python-pydocstyle-6.3
                          python-rope-1.13
                          python-lsp-jsonrpc-1.1
                          python-docstring-to-markdown)))))

(define-public python-rope-1.13
  (package
    (inherit python-rope)
    (version "1.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "rope" version))
       (sha256
        (base32
         "1078mkzivz45my8x2y5gxisr0vba630xj7yxx7anr068xhnpshsi"))))
    (arguments
     (list
      #:phases `(modify-phases %standard-phases
                  (add-after 'unpack 'disable-broken-test
                    (lambda _
                      (substitute* "ropetest/contrib/autoimporttest.py"
                        (("def test_search_module")
                         "def __notest_search_module")
                        (("def test_search_submodule")
                         "def __notest_search_submodule"))
                      (substitute* "ropetest/type_hinting_test.py"
                        (("def test_hint_or")
                         "def __notest_hint_or")))))))
    (propagated-inputs
     (list python-pytoolconfig))
    (native-inputs
     (list python-pytest-timeout
           python-pytest))))

(define-public python-pytoolconfig
  (package
    (name "python-pytoolconfig")
    (version "1.2.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pytoolconfig" version))
              (sha256
               (base32
                "18isxi4ijarl949d0zmf0b4606r6hihpi3p5yb7763m4c7ra24i5"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (add-after 'unpack 'update-license
                    (lambda _
                      (substitute* "pyproject.toml"
                        (("license-expression = (\"[^\"]*\")" all license)
                         (string-append "license = {text = " license "}")))))
                  (add-after 'unpack 'remove-mypy
                    (lambda _
                      (substitute* "pyproject.toml"
                        (("^.*mypy.*")
                         "")
                        (("strict = true") ""))))
                  (add-after 'unpack 'use-pdm-backend-instead-of-pep517
                    (lambda _
                      (substitute* "pyproject.toml"
                        (("pdm-pep517")
                         "pdm-backend")
                        (("pdm\\.pep517\\.api")
                         "pdm.backend"))))
                  (replace 'check
                    (lambda _
                      ;; Disable failing test.
                      (invoke "python" "-m" "pytest" "-k"
                              "not test_documentation"))))))
    (native-inputs (list python-pdm-backend python-tomli python-pytest python-docutils
                         python-sphinx python-tabulate))
    (propagated-inputs (list python-appdirs))
    (home-page "https://github.com/bagel897/pytoolconfig")
    (synopsis "Python Tool Configuration")
    (description "This module manages configuration for python tools,
such as rope and add support for a pyproject.toml configuration file.")
    (license license:lgpl2.1)))

(define-public python-pydocstyle-6.3
  (package
    (inherit python-pydocstyle)
    (version "6.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pydocstyle" version))
       (sha256
        (base32
         "1qa4gqmwgvakdd6j0zw0fvr6ygmpq1ib9h5r9r4hfyy81863zr3w"))))
    (propagated-inputs
     (list python-six python-snowballstemmer-2.2))))

(define-public python-snowballstemmer-2.2
  (package
    (inherit python-snowballstemmer)
    (version "2.2.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "snowballstemmer" version))
              (sha256
               (base32
                "1ccwy75i0f5yi1vy6fyvr1gf43ydhjani45mswm43ls7hpmnvc89"))))))

(define-public python-lsp-jsonrpc-1.1
  (package
    (inherit python-lsp-jsonrpc)
    (version "1.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-lsp-jsonrpc" version))
       (sha256
        (base32
         "04n95h0cqnsrdyh1gv0abh2i5ynyrq2wfqpppx9djp7mxr9y9226"))))
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-after 'unpack 'set-version
                    (lambda _ (substitute* "pyproject.toml"
                                (("dynamic = \\[\"version\"\\]")
                                 (string-append "version = \"" ,version "\""))))))))
    (build-system pyproject-build-system)
    (native-inputs
     (list python-mock python-pytest python-pytest-cov))
    (propagated-inputs
     (list python-ujson))))

(define python-black-24
  (package
    (inherit python-black)
    (version "24.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "black" version))
       (sha256
        (base32
         "0xa8vkgbvmkdh4vsk967xh81i6g47fcqf5vngdkvrqxgjx6acvl4"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f
       #:phases (modify-phases %standard-phases
                  (delete 'sanity-check))))
    (native-inputs
     (list python-hatchling-1.25
           python-hatch-fancy-pypi-readme
           python-hatch-vcs
           python-trove-classifiers-2024))))

(define-public python-lsp-black-2
  (package
    (name "python-lsp-black")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-lsp-black" version))
       (sha256
        (base32 "0pw0w024rdr60nda1c98l9mzmk3gv9584sqi7i5q8rn5239x51l2"))))
    (build-system pyproject-build-system)
    (arguments
     (list #:tests? #f))  ; No test in Pypi
    (propagated-inputs
     (list python-black-24 python-lsp-server-1.11 python-tomli))
    (native-inputs
     (list python-pytest
           python-packaging-23))
    (home-page "https://github.com/python-lsp/python-lsp-black")
    (synopsis "Black plugin for the Python LSP Server")
    (description "This package provides a plugin with support for the
@code{python-black} formatter for the Python LSP Server.")
    (license license:expat)))

(define-public python-hatchling-1.25
  (package
    (inherit python-hatchling)
    (version "1.25.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "hatchling" version))
              (sha256
               (base32
                "0qj29z3zckbk5svvfkhw8g8xcl8mv0dzzlx4a0iba416a4d66r3h"))))
    (native-inputs
     (list python-packaging-23))))

(define-public python-packaging-23
  (package
    (inherit python-packaging-bootstrap)
    (version "23.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "packaging" version))
       (sha256
        (base32
         "1ifgjb0d0bnnm78hv3mnl7hi233m7jamb2plma752djh83lv13q4"))))
    (build-system pyproject-build-system)
    (propagated-inputs
     (list python-pyparsing python-flit-core))))

(define-public python-packaging-bootstrap-23
  (package
    (inherit python-packaging-bootstrap)
    (version "23.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "packaging" version))
       (sha256
        (base32
         "1ifgjb0d0bnnm78hv3mnl7hi233m7jamb2plma752djh83lv13q4"))))
    (build-system pyproject-build-system)
    (propagated-inputs
     (list python-pyparsing python-flit-core python-six-bootstrap))))

(define-public python-trove-classifiers-2024
  (package
    (inherit python-trove-classifiers)
    (name "python-trove-classifiers")
    (version "2024.10.21.16")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/pypa/trove-classifiers.git")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0d1nqd30kvlnpcqx4plbinzp4gn25n1ymhmn8lwl43sqyk4ffq43"))))))
