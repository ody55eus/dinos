(define-module (dinos packages tmux)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public tmux-tpm
  (let ((commit "99469c4a9b1ccf77fade25842dc7bafbc8ce9946")
        (revision "2"))
    (package
      (name "tmux-tpm")
      (version (git-version "git" revision commit))
      (home-page "https://github.com/tmux-plugins/tpm")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "01ribl326n6n0qcq68a8pllbrz6mgw55kxhf9mjdc5vw01zjcvw5"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("." "share/tmux/plugins/tpm"))))
      (license license:gpl3)
      (synopsis "TMUX Plugin Manager (tpm)")
      (description ""))))
