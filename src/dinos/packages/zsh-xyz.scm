(define-module (dinos packages zsh-xyz)
  #:use-module (gnu packages base)
  #:use-module (gnu packages shells)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp))

(define-public zsh-ohmyzsh
  (let ((commit "038931039030911852d456215d6f39385d5b7a6e")
        (revision "2"))
    (package
      (name "zsh-ohmyzsh")
      (version (git-version "1.0" revision commit))
      (home-page "https://github.com/ohmyzsh/ohmyzsh")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "05xxv9ra976b6w172qdcfwpdqadr613cmbkk28q0wwi5iczc72mj"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("." "share/ohmyzsh"))))
      ;; (modify-phases %standard-phases
      ;; (add-before 'install 'remove-custom-theme
      ;; (lambda _
      ;; (delete-file-recursively "/custom/themes")))
      ;; )))
      (propagated-inputs (list zsh zsh-powerlevel))
      (license license:expat)
      (synopsis
       "Framework for managing your zsh configuration, bundled with thousands of helpful
functions, helpers, plugins, themes.")
      (description
       "A delightful community-driven (with 2,000+ contributors) framework for managing
your zsh configuration. Includes 300+ optional plugins (rails, git, macOS, hub, docker,
homebrew, node, php, python, etc), 140+ themes to spice up your morning, and an auto-update
tool so that makes it easy to keep up with the latest updates from the community."))))

(define-public zsh-completions
  (let ((commit "2f951267cde726076988bc5706d737510a88e2fb")
        (revision "2"))
    (package
      (name "zsh-completions")
      (version (git-version "1.0" revision commit))
      (home-page "https://github.com/zsh-users/zsh-completions")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "19pwi1jjrz61syf9kiw3ng64v5wka3fak9zz0ca7339p2wmw42w0"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("." "share/zsh/plugins/zsh-completion"))))
      (propagated-inputs (list zsh zsh-powerlevel))
      (license license:expat)
      (synopsis "Additional completion definitions for Zsh.")
      (description
       "This projects aims at gathering/developing new completion scripts that are not
in Zsh yet. The scripts may be contributed to the Zsh project when stable enough."))))

(define-public zsh-autocomplete
  (let ((commit "cfc3fd9a75d0577aa9d65e35849f2d8c2719b873")
        (revision "1"))
    (package
      (name "zsh-autocomplete")
      (version (git-version "1.0" revision commit))
      (home-page "https://github.com/marlonrichert/zsh-autocomplete")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "12dy6nhjfac52x7p8s3s4crv2bwl2zz5hz9f79yqwi65jigcvhs1"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("." "share/zsh/plugins/zsh-autocomplete"))))
      (propagated-inputs (list zsh))
      (license license:expat)
      (synopsis "Real-time type-ahead completion for Zsh. Asynchronous find-as-you-type
autocompletion.")
      (description
       "This plugin for Zsh adds real-time type-ahead autocompletion to your command line,
similar to what you find desktop apps. While you type on the command line, available
completions are listed automatically; no need to press any keyboard shortcuts."))))

(define-public zsh-powerlevel
  (let ((commit "aeff1153d405ebc9f60d4a8cb7afce5451c07358")
        (revision "1"))
    (package
      (name "zsh-powerlevel10k")
      (version (git-version "1.20.0" revision commit))
      (home-page "https://github.com/romkatv/powerlevel10k")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1mvw7khds3qr7vqdmk7f7nk0zjas7zs3hxz26mjxn3mdjhx6fyl9"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan '(("." "share/zsh/plugins/p10k"))))
      (license license:expat)
      (synopsis "Zsh theme with many features")
      (description
       "Powerlevel10k is a theme for Zsh. It emphasizes speed, flexibility and out-of-the-box
experience."))))
